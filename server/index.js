import express from "express";
import cors from "cors";
import session from "express-session";
import dotenv from "dotenv";
import db from "./config/Database.js";
import SequelizeStore from "connect-session-sequelize";
import http from "http";
import { Server } from "socket.io";
import UserRoute from "./routes/UserRoute.js";
import ProductRoute from "./routes/ProductRoute.js";
import ProjectRoute from "./routes/ProjectRoute.js";
import BlogRoute from "./routes/BlogRoute.js";

import SkillRoute from "./routes/SkillRoute.js";
import MessageRoute from "./routes/MessageRoute.js";
import MailRoute from "./routes/MailRoute.js";
import SocialMediaRoute from "./routes/SocialMediaRoute.js";
import AuthRoute from "./routes/AuthRoute.js";
import ProfileRoute from "./routes/ProfileRoute.js";
import SettingRoute from "./routes/SettingRoute.js";
import MessageModel from "./models/MessageModel.js";
import FileUpload from "express-fileupload";

dotenv.config();

const app = express();

const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
  db: db,
});
const server = http.createServer(app);

// (async()=>{
//     await db.sync();
// })();
app.use(
  cors({
    credentials: true,
    origin: "http://localhost:3000",
  })
);

app.use(
  session({
    secret: process.env.SESS_SECRET,
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
      secure: "auto",
    },
  })
);

app.use(express.json());

app.use(FileUpload());
app.use(express.static("public"));

app.use(UserRoute);
app.use(ProductRoute);
app.use(ProjectRoute);
app.use(BlogRoute);
app.use(SkillRoute);
app.use(MessageRoute);
app.use(MailRoute);
app.use(SocialMediaRoute);
app.use(AuthRoute);
app.use(ProfileRoute);
app.use(SettingRoute);

// // store.sync();

app.use(cors());

const io = new Server(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
});

io.on("connection", (socket) => {
  console.log(`User Connected: ${socket.id}`);

  socket.on("join_room", (data) => {
    socket.join(data);
    console.log(`User with ID: ${socket.id} joined room: ${data}`);
  });

  socket.on("send_message", async (data) => {
    socket.to(data.room).emit("receive_message", data);
    await MessageModel.create({
      senderId: data.senderId,
      receiverId: data.receiverId,
      message: data.message,
      room: data.room,
      time: data.time,
    });
  });
  socket.on("send_message", async (data) => {
    
  });

  socket.on("disconnect", () => {
    console.log("User Disconnected", socket.id);
  });
});

server.listen(process.env.APP_PORT, () => {
  console.log("Server up and running...");
});
