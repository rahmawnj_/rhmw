import express from "express";
import {
    getSettings,
    getSettingById,
    getSettingByName,
    updateSetting,
} from "../controllers/Settings.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/settings', getSettings);
router.get('/settings/:id',verifyUser, getSettingById);
router.get('/settings/:value',verifyUser, getSettingByName);
router.patch('/settings',verifyUser, updateSetting);

export default router;