import express from "express";
import {
    getMessageByRoom
} from "../controllers/Message.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/messages/:room', getMessageByRoom);

export default router;