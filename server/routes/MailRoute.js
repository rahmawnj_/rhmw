import express from "express";
import {
    getMails,
    getMailById,
    createMail,
    updateMail,
    deleteMail
} from "../controllers/mails.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/mails', getMails);
router.get('/mails/:id', getMailById);
router.post('/mails', createMail);
router.patch('/mails/:id',verifyUser, updateMail);
router.delete('/mails/:id',verifyUser, deleteMail);

export default router;