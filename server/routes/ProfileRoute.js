import express from "express";
import {
    getProfiles,
    getProfileById,
    getProfileByName,
    updateProfile,
} from "../controllers/Profile.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/profile', getProfiles);
router.get('/profile/:id',verifyUser, getProfileById);
router.get('/profile/:value',verifyUser, getProfileByName);
router.patch('/profile',verifyUser, updateProfile);

export default router;