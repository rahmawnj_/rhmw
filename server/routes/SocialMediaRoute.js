import express from "express";
import {
    getSocialMedias,
    getSocialMediaById,
    createSocialMedia,
    updateSocialMedia,
    deleteSocialMedia
} from "../controllers/socialmedias.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/socialmedias', getSocialMedias);
router.get('/socialmedias/:id', getSocialMediaById);
router.post('/socialmedias',verifyUser, createSocialMedia);
router.patch('/socialmedias/:id',verifyUser, updateSocialMedia);
router.delete('/socialmedias/:id',verifyUser, deleteSocialMedia);

export default router;