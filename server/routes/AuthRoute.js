import express from "express";
import {Login, Registration, logOut, Me} from "../controllers/Auth.js";

const router = express.Router();

router.get('/me', Me);
router.post('/login', Login);
router.post('/registration', Registration);
router.delete('/logout', logOut);

export default router;