import express from "express";
import {
    getSkills,
    getSkillById,
    createSkill,
    updateSkill,
    deleteSkill
} from "../controllers/skills.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/skills', getSkills);
router.get('/skills/:id', getSkillById);
router.post('/skills',verifyUser, createSkill);
router.patch('/skills/:id',verifyUser, updateSkill);
router.delete('/skills/:id',verifyUser, deleteSkill);

export default router;