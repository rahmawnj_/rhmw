import Mail from "../models/MailModel.js";
import { Op } from "sequelize";
import nodemailer from "nodemailer";
import Profile from "../models/ProfileModel.js";
import hbs from 'nodemailer-express-handlebars';
import path from 'path';
export const getMails = async (req, res) => {
  try {
    let response;
    response = await Mail.findAll({
      attributes: ["name", "email", "message"],
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getMailById = async (req, res) => {
  try {
    const mail = await Mail.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!mail) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;

    response = await Mail.findOne({
      attributes: ["name", "email", "message"],
      where: {
        id: mail.id,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createMail = async (req, res) => {
  const { name, email, message } = req.body;
  try {
    const profileEmail = await Profile.findOne({
      where: {
        name: 'email',
      },
    });
    const profileEmailPass = await Profile.findOne({
      where: {
        name: 'email_password',
      },
    });
    
    var transporterReceiver = nodemailer.createTransport({
      host: "smtp.gmail.com",
      auth: {
        user: profileEmail.dataValues.description,
        pass: profileEmailPass.dataValues.description
      }
    });

    transporterReceiver.use('compile', hbs({
      viewEngine: {
        extname: '.handlebars',
        partialsDir: path.resolve('./views'),
        defaultLayout: false
      },
      viewPath: path.resolve('./views'),
      extName: '.handlebars'
    }))
    var mailOptionsReceiver = {
      from: profileEmail.dataValues.description,
      to: profileEmail.dataValues.description,
      subject: 'Email Notification',
      template: 'main',
      context: {
        title: 'Email Notification',
        body: `${name} sent you an email : ${message}`
      }
    };
        
    transporterReceiver.sendMail(mailOptionsReceiver, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
    // =======
    var transporterSender = nodemailer.createTransport({
      host: "smtp.gmail.com",
      auth: {
        user: profileEmail.dataValues.description,
        pass: profileEmailPass.dataValues.description
      }
    });
    transporterSender.use('compile', hbs({
      viewEngine: {
        extname: '.handlebars',
        partialsDir: path.resolve('./views'),
        defaultLayout: false
      },
      viewPath: path.resolve('./views'),
      extName: '.handlebars'
    }))
    var mailOptionsSender = {
      from: profileEmail.dataValues.description,
      to: email,
      subject: 'Email Notification',
      template: 'main',
      context: {
        title: 'Email Notification',
        body: `Hi ${name}, Email sent Successfully. I will reply ASAP! Thankyou!`
      }
    };
    
    transporterSender.sendMail(mailOptionsSender, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });

    await Mail.create({
      name: name,
      email: email,
      message: message,
    });

    res.status(201).json({ msg: "Mail Created Successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const updateMail = async (req, res) => {
  try {
    const mail = await Mail.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!mail) return res.status(404).json({ msg: "Data tidak ditemukan" });
    const { name, email, message } = req.body;

    await Mail.update(
      { name, email, message },
      {
        where: {
          id: mail.id,
        },
      }
    );

    res.status(200).json({ msg: "Mail updated successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const deleteMail = async (req, res) => {
  try {
    const mail = await Mail.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!mail) return res.status(404).json({ msg: "Data tidak ditemukan" });

    await createMail.destroy({
      where: {
        id: mail.id,
      },
    });

    res.status(200).json({ msg: "Mail deleted successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
