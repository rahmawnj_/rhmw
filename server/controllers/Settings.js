import Setting from "../models/SettingModel.js";
import { Op } from "sequelize";

export const getSettings = async (req, res) => {
  try {
    let response;
    response = await Setting.findAll({
      attributes: ["name", "description"],
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getSettingById = async (req, res) => {
  try {
    const setting = await Setting.findOne({
      where: {
        id: req.params.id,
      },
    });
    if (!setting) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;

    response = await Setting.findOne({
      attributes: ["name", "description"],
      where: {
        id: setting.id,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
export const getSettingByName = async (req, res) => {
  try {
    const setting = await Setting.findOne({
      where: {
        name: req.params.name,
      },
    });
    if (!setting) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;
    response = await Setting.findOne({
      attributes: ["name", "description"],
      where: {
        name: setting.name,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const updateSetting = async (req, res) => {
  try {


    const {socketId} = req.body;

    const settingSocketId = await Setting.findOne({
      where: {
        name: 'socketId',
      },
    });
    await Setting.update(
      {
        description: socketId,
      },
      {
        where: {
          id: settingSocketId.dataValues.id,
        },
      }
    );

    res.status(200).json({ msg: "Setting Updated Successfuly" });
  } catch (error) {
    console.log(error.message);
  }
};