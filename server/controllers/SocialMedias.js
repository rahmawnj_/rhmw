import SocialMedia from "../models/SocialMediaModel.js";
import { Op } from "sequelize";

export const getSocialMedias = async (req, res) => {
  try {
    let response;
    response = await SocialMedia.findAll({
      attributes: ["uuid", "platform", "link", "icon", "status"],
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getSocialMediaById = async (req, res) => {
  try {
    const socialmedia = await SocialMedia.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!socialmedia) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;

    response = await SocialMedia.findOne({
      attributes: ["uuid", "platform", "link", "icon", "status"],
      where: {
        id: socialmedia.id,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createSocialMedia = async (req, res) => {
  const { platform, link, status, icon } = req.body;
  try {
    await SocialMedia.create({
      platform: platform,
      link: link,
      status: status,
      icon: icon,
    });
    res.status(201).json({ msg: "SocialMedia Created Successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const updateSocialMedia = async (req, res) => {
  try {
    const socialmedia = await SocialMedia.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!socialmedia) return res.status(404).json({ msg: "Data tidak ditemukan" });
    const { platform, link, status, icon } = req.body;

    await SocialMedia.update(
      { platform, link, status, icon },
      {
        where: {
          id: socialmedia.id,
        },
      }
    );

    res.status(200).json({ msg: "SocialMedia updated successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const deleteSocialMedia = async (req, res) => {
  try {
    const socialmedia = await SocialMedia.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!socialmedia) return res.status(404).json({ msg: "Data tidak ditemukan" });

    await SocialMedia.destroy({
      where: {
        id: socialmedia.id,
      },
    });

    res.status(200).json({ msg: "SocialMedia deleted successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
