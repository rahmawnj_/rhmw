import Blog from "../models/BlogModel.js";
import { Op } from "sequelize";
import path from "path";
import fs from "fs";
export const getBlogs = async (req, res) => {
  try {
    let response;
    response = await Blog.findAll();

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getBlogById = async (req, res) => {
  try {
    const blog = await Blog.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!blog) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;

    response = await Blog.findOne({
      attributes: ["uuid", "title", "description", "tags", "status", "image"],
      where: {
        id: blog.id,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createBlog = async (req, res) => {
  if (req.files === null)
    return res.status(400).json({ msg: "No File Uploaded" });
  const title = req.body.title;
  const description = req.body.description;
  const tags = req.body.tags;
  const status = req.body.status;
  const file = req.files.file;
  const fileSize = file.data.length;
  const ext = path.extname(file.name);
  const fileName = file.md5 + ext;
  const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;
  const allowedType = ['.png','.jpg','.jpeg'];
  if (!allowedType.includes(ext.toLowerCase()))
    return res.status(422).json({ msg: "Invalid Images" });
  if (fileSize > 5000000)
    return res.status(422).json({ msg: "Image must be less than 5 MB" });

  file.mv(`./public/images/blogs/${fileName}`, async (err) => {
    if (err) return res.status(500).json({ msg: err.message });
    try {
      await Blog.create({
        title: title,
        description: description,
        tags: tags,
        status: status,
        image: fileName,
      });
      res.status(201).json({ msg: "Blog Created Successfuly" });
    } catch (error) {
      console.log(error.message);
    }
  });
};

export const updateBlog = async (req, res) => {
  const blog = await Blog.findOne({
    where: {
      uuid: req.params.id,
    },
  });
  if (!blog) return res.status(404).json({ msg: "Data tidak ditemukan" });

  let fileName = "";
  if (req.files === null) {
    fileName = blog.image;
  } else {
    const file = req.files.file;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    fileName = file.md5 + ext;
    const allowedType = [".png", ".jpg", ".jpeg"];

    if (!allowedType.includes(ext.toLowerCase()))
      return res.status(422).json({ msg: "Invalid Images" });
    if (fileSize > 5000000)
      return res.status(422).json({ msg: "Image must be less than 5 MB" });

    const filepath = `./public/images/blogs/${blog.image}`;
    fs.unlinkSync(filepath);

    file.mv(`./public/images/blogs/${fileName}`, (err) => {
      if (err) return res.status(500).json({ msg: err.message });
    });
  }
  const name = req.body.name;
  const description = req.body.description;
  const tags = req.body.tags;
  const status = req.body.status;
  const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;

  try {
    await Blog.update(
      {
        name: name,
        description: description,
        tags: tags,
        status: status,
        image: fileName,
      },
      {
        where: {
          uuid: req.params.id,
        },
      }
    );
    res.status(200).json({ msg: "Blog Updated Successfuly" });
  } catch (error) {
    console.log(error.message);
  }
};
export const deleteBlog = async (req, res) => {
  const blog = await Blog.findOne({
    where: {
      uuid: req.params.id,
    },
  });
  if (!blog) return res.status(404).json({ msg: "No Data Found" });

  try {
    const filepath = `./public/images/blogs/${blog.image}`;
    fs.unlinkSync(filepath);
    await Blog.destroy({
      where: {
        uuid: req.params.id,
      },
    });
    res.status(200).json({ msg: "Blog Deleted Successfuly" });
  } catch (error) {
    console.log(error.message);
  }
};
