import Message from "../models/MessageModel.js";
import { Op } from "sequelize";

export const getMessageByRoom = async (req, res) => {
  try {
    const message = await Message.findAll({
      where: {
        room: req.params.room,
      },
    });
    if (!message) return res.status(404).json({ msg: "Data tidak ditemukan" });

    res.status(200).json(message);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
