import Profile from "../models/ProfileModel.js";
import { Op } from "sequelize";
import path from "path";
import fs from "fs";

export const getProfiles = async (req, res) => {
  try {
    let response;
    response = await Profile.findAll({
      attributes: ["name", "description"],
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getProfileById = async (req, res) => {
  try {
    const profile = await Profile.findOne({
      where: {
        id: req.params.id,
      },
    });
    if (!profile) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;

    response = await Profile.findOne({
      attributes: ["name", "description"],
      where: {
        id: profile.id,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
export const getProfileByName = async (req, res) => {
  try {
    const profile = await Profile.findOne({
      where: {
        name: req.params.name,
      },
    });
    if (!profile) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;
    response = await Profile.findOne({
      attributes: ["name", "description"],
      where: {
        name: profile.name,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const updateProfile = async (req, res) => {
  const profileFile = await Profile.findOne({
    where: {
      name: 'image',
    },
  });
  if (!profileFile) return res.status(404).json({ msg: "Data tidak ditemukan" });

  let fileName = "";
  if (req.files === null) {
    fileName = profileFile.dataValues.description;
  } else {
    const file = req.files.file;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    fileName = file.md5 + ext;
    const allowedType = [".png", ".jpg", ".jpeg"];

    if (!allowedType.includes(ext.toLowerCase()))
      return res.status(422).json({ msg: "Invalid Images" });
    if (fileSize > 5000000)
      return res.status(422).json({ msg: "Image must be less than 5 MB" });

    const filepath = `./public/images/${profileFile.dataValues.description}`;
    fs.unlinkSync(filepath);

    file.mv(`./public/images/profile/${fileName}`, (err) => {
      if (err) return res.status(500).json({ msg: err.message });
    });
  }
  try {

    await Profile.update(
      {
        description: fileName,
      },
      {
        where: {
          id: profileFile.dataValues.id,
        },
      }
    );
    const {name, motto, role, about, location, email, contact, emailPass} = req.body;

    const profileName = await Profile.findOne({
      where: {
        name: 'name',
      },
    });
    await Profile.update(
      {
        description: name,
      },
      {
        where: {
          id: profileName.dataValues.id,
        },
      }
    );
    const profileMotto = await Profile.findOne({
      where: {
        name: 'motto',
      },
    });
    await Profile.update(
      {
        description: motto,
      },
      {
        where: {
          id: profileMotto.dataValues.id,
        },
      }
    );
    const profileAbout = await Profile.findOne({
      where: {
        name: 'about',
      },
    });
    await Profile.update(
      {
        description: about,
      },
      {
        where: {
          id: profileAbout.dataValues.id,
        },
      }
    );
    const profileRole = await Profile.findOne({
      where: {
        name: 'role',
      },
    });
    await Profile.update(
      {
        description: role,
      },
      {
        where: {
          id: profileRole.dataValues.id,
        },
      }
    );
    const profileLocation = await Profile.findOne({
      where: {
        name: 'location',
      },
    });
    await Profile.update(
      {
        description: location,
      },
      {
        where: {
          id: profileLocation.dataValues.id,
        },
      }
    );
    const profileEmail = await Profile.findOne({
      where: {
        name: 'email',
      },
    });
    await Profile.update(
      {
        description: email,
      },
      {
        where: {
          id: profileEmail.dataValues.id,
        },
      }
    );
    const profileEmailPass = await Profile.findOne({
      where: {
        name: 'email_password',
      },
    });
    await Profile.update(
      {
        description: emailPass,
      },
      {
        where: {
          id: profileEmailPass.dataValues.id,
        },
      }
    );
    const profileContact = await Profile.findOne({
      where: {
        name: 'contact',
      },
    });
    await Profile.update(
      {
        description: contact,
      },
      {
        where: {
          id: profileContact.dataValues.id,
        },
      }
    );
    res.status(200).json({ msg: "Profile Updated Successfuly" });
  } catch (error) {
    console.log(error.message);
  }
};