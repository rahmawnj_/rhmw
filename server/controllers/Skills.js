import Skill from "../models/SkillModel.js";
import { Op } from "sequelize";

export const getSkills = async (req, res) => {
  try {
    let response;
    response = await Skill.findAll({
      attributes: ["uuid", "name", "description", "progress"],
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getSkillById = async (req, res) => {
  try {
    const skill = await Skill.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!skill) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;

    response = await Skill.findOne({
      attributes: ["uuid", "name", "description", "progress"],
      where: {
        id: skill.id,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createSkill = async (req, res) => {
  const { name, description, progress } = req.body;
  try {
    await Skill.create({
      name: name,
      description: description,
      progress
    });
    res.status(201).json({ msg: "Skill Created Successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const updateSkill = async (req, res) => {
  try {
    const skill = await Skill.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!skill) return res.status(404).json({ msg: "Data tidak ditemukan" });
    const { name, description, progress } = req.body;

    await Skill.update(
      { name, description, progress },
      {
        where: {
          id: skill.id,
        },
      }
    );

    res.status(200).json({ msg: "Skill updated successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const deleteSkill = async (req, res) => {
  try {
    const skill = await Skill.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!skill) return res.status(404).json({ msg: "Data tidak ditemukan" });

    await Skill.destroy({
      where: {
        id: skill.id,
      },
    });

    res.status(200).json({ msg: "Skill deleted successfuly" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
