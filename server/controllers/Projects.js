import Project from "../models/ProjectModel.js";
import { Op } from "sequelize";
import path from "path";
import fs from "fs";

export const getProjects = async (req, res) => {
  try {
    let response;
    response = await Project.findAll({
      attributes: ["uuid", "name", "description", "tags", "status", "image"],
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getProjectById = async (req, res) => {
  try {
    const project = await Project.findOne({
      where: {
        uuid: req.params.id,
      },
    });
    if (!project) return res.status(404).json({ msg: "Data tidak ditemukan" });
    let response;

    response = await Project.findOne({
      attributes: ["uuid", "name", "description", "tags", "status", "image"],
      where: {
        id: project.id,
      },
    });

    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const createProject = async (req, res) => {
  
  if (req.files === null)
    return res.status(400).json({ msg: "No File Uploaded" });
  const name = req.body.name;
  const description = req.body.description;
  const tags = req.body.tags;
  const status = req.body.status;
  const file = req.files.file;
  const fileSize = file.data.length;
  const ext = path.extname(file.name);
  const fileName = file.md5 + ext;
  const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;
  const allowedType = ['.png','.jpg','.jpeg'];
  if (!allowedType.includes(ext.toLowerCase()))
    return res.status(422).json({ msg: "Invalid Images" });
  if (fileSize > 5000000)
    return res.status(422).json({ msg: "Image must be less than 5 MB" });

  file.mv(`./public/images/projects/${fileName}`, async (err) => {
    if (err) return res.status(500).json({ msg: err.message });
    try {
      await Project.create({
        name: name,
        description: description,
        tags: tags,
        status: status,
        image: fileName,
      });
      res.status(201).json({ msg: "Project Created Successfuly" });
    } catch (error) {
      console.log(error.message);
    }
  });
};

export const updateProject = async (req, res) => {
  const project = await Project.findOne({
    where: {
      uuid: req.params.id,
    },
  });
  console.log("PROJEK", project);
  if (!project) return res.status(404).json({ msg: "Data tidak ditemukan" });

  let fileName = "";
  if (req.files === null) {
    fileName = project.image;
  } else {
    const file = req.files.file;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    fileName = file.md5 + ext;
    const allowedType = [".png", ".jpg", ".jpeg"];

    if (!allowedType.includes(ext.toLowerCase()))
      return res.status(422).json({ msg: "Invalid Images" });
    if (fileSize > 5000000)
      return res.status(422).json({ msg: "Image must be less than 5 MB" });

    const filepath = `./public/images/projects/${project.image}`;
    fs.unlinkSync(filepath);

    file.mv(`./public/images/projects/${fileName}`, (err) => {
      if (err) return res.status(500).json({ msg: err.message });
    });
  }
  const name = req.body.name;
  const description = req.body.description;
  const tags = req.body.tags;
  const status = req.body.status;
  const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;

  try {
    await Project.update(
      {
        name: name,
        description: description,
        tags: tags,
        status: status,
        image: fileName,
      },
      {
        where: {
          uuid: req.params.id,
        },
      }
    );
    res.status(200).json({ msg: "Project Updated Successfuly" });
  } catch (error) {
    console.log(error.message);
  }
};

export const deleteProject = async (req, res) => {
  const project = await Project.findOne({
    where: {
      uuid: req.params.id,
    },
  });
  if (!project) return res.status(404).json({ msg: "No Data Found" });

  try {
    const filepath = `./public/images/projects/${project.image}`;
    fs.unlinkSync(filepath);
    await Project.destroy({
      where: {
        uuid: req.params.id,
      },
    });
    res.status(200).json({ msg: "Project Deleted Successfuly" });
  } catch (error) {
    console.log(error.message);
  }
};
