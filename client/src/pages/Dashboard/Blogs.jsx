import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import Layout from "../../components/dashboard/Layout";

import { getMe } from "../../features/authSlice";

const Blogs = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);
  const [blogs, setBlogs] = useState([]);
  const [msg, setMsg] = useState("");

  useEffect(() => {
    async function getBlogs() {
      const response = await axios.get("http://localhost:5000/blogs");
      setBlogs(response.data);
    }
    getBlogs();
  }, []);

  const deleteBlog = async (blogId) => {
    await axios.delete(`http://localhost:5000/blogs/${blogId}`);
    const response = await axios.get("http://localhost:5000/blogs");
    setBlogs(response.data);
    setMsg(`<div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
    Blog has been deleted successfully
  </div>`);
  };

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);
  return (
    <Layout title={'Blog'}>
    <div className="pt-6 px-4">
       <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
         <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
           <div dangerouslySetInnerHTML={{ __html: msg }} />

           <div className="rounded-t mb-0 px-4 py-3 border-0">
             <div className="flex flex-wrap items-center">
               <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                 <h3 className="font-semibold text-base text-blueGray-700">
                   Blogs
                 </h3>
               </div>
               <div className="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
                 <Link
                   className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase p-2 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                   to={"/admin/blogs/add"}
                 >
                   <i className="fas fa-plus" />
                 </Link>
               </div>
             </div>
           </div>

           <div className="block w-full overflow-x-auto">
             <table className="items-center bg-transparent w-full border-collapse ">
               <thead>
                 <tr>
                   <th className="px-3 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                     #
                   </th>
                   <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                     Image
                   </th>
                   <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                     Title
                   </th>
                   <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                     Status
                   </th>
                   <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left"></th>
                 </tr>
               </thead>

               <tbody>
                 {blogs.length !== 0 ? (
                   blogs.map((blog, index) => (
                     <tr key={index}>
                       <th className="border-t-0 px-3 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                         {index + 1}
                       </th>
                       <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 ">
                       <figure
                           className="overflow-hidden"
                           style={{ width: "100px" }}
                         >
                           <img
                             src={`http://localhost:5000/images/blogs/${blog.image}`}
                             alt={blog.title}
                             style={{ height: "50px" }}
                           />
                         </figure>
                       </td>
                       <td className="border-t-0 px-6 align-center border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                         {blog.title}
                       </td>
                       <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                         <span
                           className={`p-1 rounded-sm ${
                             blog.status === "Publish"
                               ? "bg-blue-200"
                               : "bg-red-200"
                           }`}
                         >
                           {blog.status}
                         </span>
                       </td>
                       <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                         <Link
                           to={`/admin/blogs/detail/${blog.uuid}`}
                           className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase p-2 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                         >
                           <i className="fas fa-eye"></i>
                         </Link>
                         <Link
                           to={`/admin/blogs/edit/${blog.uuid}`}
                           className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase p-2 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                         >
                           <i className="fas fa-pencil-alt"></i>
                         </Link>
                         <button
                           className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase p-2 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                           onClick={() => deleteBlog(blog.uuid)}
                         >
                           <i className="fas fa-trash"></i>
                         </button>
                       </td>
                     </tr>
                   ))
                 ) : (
                   <tr>
                   <td colSpan="5" className="text-center">
                     No Blog Found!
                   </td>
                 </tr>
                 )}
               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
   </Layout>
  );
};

export default Blogs;
