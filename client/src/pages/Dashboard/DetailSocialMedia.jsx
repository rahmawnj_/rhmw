import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams, Link } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import axios from "axios";
import { Helmet } from "react-helmet";
const EditSocialMedia = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);
  const [platform, setPlatform] = useState("");
  const [icon, setIcon] = useState("");
  const [status, setStatus] = useState("");
  const [link, setLink] = useState("");
  const [msg, setMsg] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const getSocialMediaById = async () => {
      try {
        const response = await axios.get(
          `http://localhost:5000/socialmedias/${id}`
        );
        setPlatform(response.data.platform);
        setIcon(response.data.icon);
        setStatus(response.data.status);
        setLink(response.data.link);
      } catch (error) {
        if (error.response) {
          setMsg(`
          <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
        ${error.response.data.msg}
          </div>
        `);
        }
      }
    };
    getSocialMediaById();
  }, [id]);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);
  return (
    <Layout>
      <div className="px-4">
        <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
          <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
            <div dangerouslySetInnerHTML={{ __html: msg }} />
            <h1>
              <i className={`fa-brands ${icon}`}></i> {platform}
            </h1>
            <Link to={link}>{link}</Link>
            <span
              className={`${
                status === "Publish" ? "text-blue-600" : "text-red-600"
              }`}
            >
              {status}
            </span>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default EditSocialMedia;
