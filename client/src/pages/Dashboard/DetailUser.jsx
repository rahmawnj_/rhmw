import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import axios from "axios";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
const DetailUser = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState("");
  const [msg, setMsg] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/users/${id}`);
        setName(response.data.name);
        setEmail(response.data.email);
        setRole(response.data.role);
      } catch (error) {
        if (error.response) {
          setMsg(`
          <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
        ${error.response.data.msg}
          </div>
        `);
        }
      }
    };
    getUserById();
  }, [id]);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);
  return (
    <Layout title={'User'}>
    <div className="px-4">
      <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
        <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
          <div dangerouslySetInnerHTML={{ __html: msg }} />
          <h1>
            {name}
          </h1>
          <span>{email}</span>
          <span
            className={`${
              role === "admin" ? "text-blue-600" : "text-yellow-600"
            }`}
          >
            {role}
          </span>
        </div>
      </div>
    </div>
  </Layout>
  );
};

export default DetailUser;
