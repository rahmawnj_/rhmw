import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import axios from "axios";
const EditSkill = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [progress, setProgress] = useState("");

  const [msg, setMsg] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const getSkillById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/skills/${id}`);
        setName(response.data.name);
        setDescription(response.data.description);
        setProgress(response.data.progress);
      } catch (error) {
        if (error.response) {
          setMsg(`
          <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
        ${error.response.data.msg}
          </div>
        `);
        }
      }
    };
    getSkillById();
  }, [id]);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);
  return (
    <Layout title={'Skill'}>
      <div className="px-4">
        <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
          <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
            <div dangerouslySetInnerHTML={{ __html: msg }} />
            <h1>{name}</h1>
            <div className="w-full bg-gray-200 h-1">
              <div
                className="bg-blue-600 h-1"
                style={{ width: progress + "%" }}
              />
            </div>
            <p>{description}</p>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default EditSkill;
