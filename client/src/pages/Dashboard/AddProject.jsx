import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import axios from "axios";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import { convertToHTML } from "draft-convert";
const AddProject = () => {
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [file, setFile] = useState("");
  const [preview, setPreview] = useState("");
  const [msg, setMsg] = useState("");
  const [tags, setTags] = useState([]);
  const [description, setDescription] = useState(null);
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const handleEditorChange = (state) => {
    setEditorState(state);
    convertContentToHTML();
  };
  const convertContentToHTML = () => {
    let currentContentAsHTML = convertToHTML(editorState.getCurrentContent());
    setDescription(currentContentAsHTML);
  };

  function handleKeyDown(e) {
    if (e.key !== "Enter") return;
    const value = e.target.value;
    if (!value.trim()) return;
    setTags([...tags, value]);
    e.target.value = "";
  }

  function removeTag(index) {
    setTags(tags.filter((el, i) => i !== index));
  }

  const loadImage = (e) => {
    const image = e.target.files[0];
    setFile(image);
    setPreview(URL.createObjectURL(image));
  };
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);

  const saveProject = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description);
    formData.append("tags", tags);
    formData.append("status", status);
    formData.append("file", file);
    try {
      await axios.post("http://localhost:5000/projects", formData);
      navigate("/admin/projects");
    } catch (error) {
      if (error.response) {
        setMsg(`
        <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
      ${error.response.data.msg}
        </div>
      `);
      }
    }
  };

  return (
    <Layout title={'Project'}>
      <div className="px-4">
        <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
          <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
            <div dangerouslySetInnerHTML={{ __html: msg }} />
            {preview ? (
              <div className="grid place-items-center">
                <figure style={{ width: "400px" }}>
                  <img src={preview} alt="project" className="h-200" />
                </figure>
              </div>
            ) : (
              <div className="grid place-items-center">
                <figure style={{ width: "400px" }}>
                  <img src="/logo192.png" alt="project" className="h-200" />
                </figure>
              </div>
            )}

            <form onSubmit={saveProject}>
              <div className="mb-5">
                <input
                  onChange={loadImage}
                  type="file"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Name"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Project Name
                </label>
                <input
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  type="text"
                  name="Name"
                  id="Name"
                  placeholder="Name"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Status"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Status
                </label>
                <select
                  id="Status"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                >
                  <option defaultValue={""} disabled>
                    Select one
                  </option>
                  <option value="Publish">Publish</option>
                  <option value="Draft">Draft</option>
                </select>
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Description"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Description
                </label>
                <Editor
                  editorState={editorState}
                  onEditorStateChange={handleEditorChange}
                  wrapperClassName="wrapper-class"
                  editorClassName="editor-class"
                  toolbarClassName="toolbar-class"
                  className="form-control"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Tags"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Tags
                </label>
                <div className="mb-2">
                  {tags.map((tag, index) => (
                    <label
                      className="bg-blue-300 m-1 rounded-md p-1"
                      key={index}
                    >
                      <span className="font-bold">{tag}</span>
                      <span
                        className="p-1 font-bol"
                        onClick={() => removeTag(index)}
                      >
                        &times;
                      </span>
                    </label>
                  ))}
                </div>

                <textarea
                  onKeyDown={handleKeyDown}
                  type="text"
                  name="Tags"
                  id="Tags"
                  placeholder="Tags"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div>
                <button
                  type="submit"
                  className="hover:shadow-form rounded-md bg-[#6A64F1] py-3 px-8 text-base font-semibold text-white outline-none"
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default AddProject;
