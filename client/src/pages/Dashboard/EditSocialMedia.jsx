import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import axios from "axios";
import { Helmet } from "react-helmet";
const EditSocialMedia = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);
  const [platform, setPlatform] = useState("");
  const [icon, setIcon] = useState("");
  const [status, setStatus] = useState("");
  const [link, setLink] = useState("");
  const [msg, setMsg] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const getSocialMediaById = async () => {
      try {
        const response = await axios.get(
          `http://localhost:5000/socialmedias/${id}`
        );
        setPlatform(response.data.platform);
        setIcon(response.data.icon);
        setStatus(response.data.status);
        setLink(response.data.link);
      } catch (error) {
        if (error.response) {
          setMsg(`
        <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
      ${error.response.data.msg}
        </div>
      `);
        }
      }
    };
    getSocialMediaById();
  }, [id]);

  const updateSocialMedia = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(`http://localhost:5000/socialmedias/${id}`, {
        platform: platform,
        icon: icon,
        link: link,
        status: status,
      });
      navigate("/admin/socialmedias");
    } catch (error) {
      if (error.response) {
        setMsg(`
        <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
      ${error.response.data.msg}
        </div>
      `);
      }
    }
  };
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);
  return (
    <Layout title={'Social Media'}>
      <div className="px-4">
        <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
          <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
            <div dangerouslySetInnerHTML={{ __html: msg }} />

            <form onSubmit={updateSocialMedia}>
              <div className="mb-5">
                <label
                  htmlFor="Platform"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Platform
                </label>
                <input
                  value={platform}
                  onChange={(e) => setPlatform(e.target.value)}
                  type="text"
                  name="Platform"
                  id="Platform"
                  placeholder="Platform"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Link"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Link
                </label>
                <input
                  value={link}
                  onChange={(e) => setLink(e.target.value)}
                  type="text"
                  name="Link"
                  id="Link"
                  placeholder="Link"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Status"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Status
                </label>
                <select
                  id="Status"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                >
                  <option defaultValue={""} disabled>
                    Select one
                  </option>
                  <option value="Publish">Publish</option>
                  <option value="Draft">Draft</option>
                </select>
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Icon"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Icon
                </label>
                <input
                  value={icon}
                  onChange={(e) => setIcon(e.target.value)}
                  type="text"
                  name="Icon"
                  id="Icon"
                  placeholder="Icon"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              {/* <div className="mb-5">
                <label
                  htmlFor="message"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Message
                </label>
                <textarea
                  rows={4}
                  name="message"
                  id="message"
                  placeholder="Type your message"
                  className="w-full resize-none rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                  defaultValue={""}
                />
              </div> */}
              <div>
                <button type="submit" className="hover:shadow-form rounded-md bg-[#6A64F1] py-3 px-8 text-base font-semibold text-white outline-none">
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </Layout>
  );
};

export default EditSocialMedia;
