import { useEffect, useState } from "react";
import React from "react";
import axios from "axios";
const Chat = ({ socket, name, room, receiverName }) => {
  const [currentMessage, setCurrentMessage] = useState();
  const [messageList, setMessageList] = useState([]);

  const sendMessage = async () => {
    if (currentMessage !== "") {
      const socketId = await axios.get(`http://localhost:5000/settings/1`);

      const messageData = {
        room: room,
        author: name,
        senderId: socketId.data.description,
        receiverId: room,
        message: currentMessage,
        time:
          new Date(Date.now()).getHours() +
          ":" +
          new Date(Date.now()).getMinutes(),
      };
      await socket.emit("send_message", messageData);
      setMessageList((list) => [...list, messageData]);
      setCurrentMessage("");
    }
  };

  useEffect(() => {
    socket.on("receive_message", (data) => {
      setMessageList((list) => [...list, data]);
    });
  }, [socket]);
  useEffect(() => {
    const getMessages = async () => {
      setMessageList(() => []);
      const response = await axios.get(
        `http://localhost:5000/messages/${room}`
      );
      response.data.forEach((data) => {
        const messageData = {
          room: data.room,
          author: data.senderId,
          message: data.message,
          time: data.time,
        };
        setMessageList((list) => [...list, messageData]);
      });
    };
    getMessages();
  }, [room]);

  return (
      <div className="w-full">
        <div className="relative flex items-center p-3 border-b border-gray-300">
          <span className="block ml-2 font-bold text-gray-600">
            {receiverName}
          </span>
          <span className="absolute w-3 h-3 bg-green-600 rounded-full left-0 top-3"></span>
        </div>
        <div className="bg-blue-100 ">
          <div className="relative w-full p-6 overflow-y-auto h-[40rem]">
            <ul className="space-y-2">
             
              {messageList.map((messageContent, index) => {
              return (
                 <li 
                 key={index}
                 className={`flex ${
                  room !== messageContent.author ? "justify-end" : "justify-start"

                 }`}>
                 <div className="relative bg-white max-w-xl px-4 py-2 text-gray-700 rounded shadow">
                   <span className="block">{messageContent.message}</span>
                 </div>
                 <span className="grid content-end text-gray-500 font-sm ml-1">{messageContent.time}</span>
               </li>
              );
            })}
            </ul>
          </div>
        </div>

        <div className="flex text-center content-center items-center h-20 overflow-hidden justify-between w-full p-3 border-t border-gray-300">
          <input
            type="text"
            placeholder="Message"
            className="block w-full py-2 pl-4 mx-3 bg-gray-100 rounded-full outline-none focus:text-gray-700"
            name="message"
            required
            value={currentMessage}
            onChange={(event) => {
              setCurrentMessage(event.target.value);
            }}
            onKeyPress={(event) => {
              event.key === "Enter" && sendMessage();
            }}
          />
   
          <button type="submit" onClick={sendMessage}>
            <svg
              className="w-5 h-5 text-gray-500 origin-center transform rotate-90"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
            </svg>
          </button>
        </div>
      </div>
  );
};

export default Chat;
