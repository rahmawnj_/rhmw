import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import { Helmet } from "react-helmet";
const Settings = () => {
  const [socketId, setSocketId] = useState("");
  const { isError, user } = useSelector((state) => state.auth);

  const [msg, setMsg] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    const getSetting = async () => {
      const response = await axios.get("http://localhost:5000/settings");
      setSocketId(response.data[0].description);
      console.log(socketId);
    };
    getSetting();
  }, []);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);

  const updateSettings = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("socketId", socketId);

    try {
      await axios.patch(`http://localhost:5000/settings`, formData);
      navigate("/admin/settings");
      setMsg(`
      <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
   Settings Successfully Updated
      </div>
    `);
    } catch (error) {
      if (error.response) {
        setMsg(`
        <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
      ${error.response.data.msg}
        </div>
      `);
      }
    }
  };
  return (
    <Layout title="Setting">
        <div className="px-4">
        <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
          <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
            <div dangerouslySetInnerHTML={{ __html: msg }} />

            <form onSubmit={updateSettings}>
              <div className="mb-5">
                <label
                  htmlFor="SocketId"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  SocketId
                </label>
                <input
                  value={socketId}
                  onChange={(e) => setSocketId(e.target.value)}
                  type="text"
                  name="SocketId"
                  id="SocketId"
                  placeholder="SocketId"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div>
                <button
                  type="submit"
                  className="hover:shadow-form rounded-md bg-[#6A64F1] py-3 px-8 text-base font-semibold text-white outline-none"
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Settings;
