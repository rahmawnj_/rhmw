import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import axios from "axios";
import { Helmet } from "react-helmet";
const AddSkill = () => {

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [progress, setProgress] = useState("");
  const [msg, setMsg] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);

 
  const saveSkill = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/skills", {
        name: name,
        description: description,
        progress: progress,
      });
      navigate("/admin/skills");
    } catch (error) {
      if (error.response) {
        setMsg(`
          <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
        ${error.response.data.msg}
          </div>
        `);
      }
    }
  };


  return (
    <Layout title={'Skill'}>
    <div className="px-4">
      <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
        <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
          <div dangerouslySetInnerHTML={{ __html: msg }} />

          <form onSubmit={saveSkill}>
            <div className="mb-5">
              <label
                htmlFor="Name"
                className="mb-3 block text-base font-medium text-[#07074D]"
              >
                Name
              </label>
              <input
                value={name}
                onChange={(e) => setName(e.target.value)}
                type="text"
                name="Name"
                id="Name"
                placeholder="Name"
                className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
              />
            </div>
            <div className="mb-5">
              <label
                htmlFor="Progress"
                className="mb-3 block text-base font-medium text-[#07074D]"
              >
                Progress
              </label>
              <input
                value={progress}
                onChange={(e) => setProgress(e.target.value)}
                name="Progress"
                id="Progress"
                placeholder="Progress"
                type="range"
                className="
                bg-blue-300
                  form-range
                  appearance-none
                  w-full
                  h-6
                  p-0
                  bg-transparent
                  focus:outline-none focus:ring-0 focus:shadow-none
                "
                min="0"
                max="5"
                step="0.5"
              />
            </div>
        
            <div className="mb-5">
              <label
                htmlFor="Description"
                className="mb-3 block text-base font-medium text-[#07074D]"
              >
                Description
              </label>
              <textarea
                rows={4}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                name="Description"
                id="Description"
                placeholder="Description"
                className="w-full resize-none rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                defaultValue={""}
              />
            </div>
            <div>
              <button type="submit" className="hover:shadow-form rounded-md bg-[#6A64F1] py-3 px-8 text-base font-semibold text-white outline-none">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </Layout>
  );
};

export default AddSkill;
