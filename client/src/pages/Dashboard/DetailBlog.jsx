import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams, Link } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import axios from "axios";
import { Helmet } from "react-helmet";
const EditBlog = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState("");
  const [tags, setTags] = useState([]);
  const [image, setImage] = useState("");
  const [msg, setMsg] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const getBlogById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/blogs/${id}`);
        setTitle(response.data.title);
        setDescription(response.data.description);
        setStatus(response.data.status);
        const Tags = response.data.tags;
        const newTags = Tags.split(",");
        setTags(newTags);
        setImage(response.data.image);
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getBlogById();
  }, [id]);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);
  return (
    <Layout title={'Blog'}>
    <div className="px-4">
      <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
        <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
          <div dangerouslySetInnerHTML={{ __html: msg }} />
          <div className="grid place-items-center">
            <figure style={{ width: "400px" }}>
              <img
                src={`http://localhost:5000/images/projects/${image}`}
                alt="project"
                className="h-200"
              />
            </figure>
          </div>
          <h1 className="font-bold">{title}</h1>

          <div>
            <label for="status">Status : </label>
            <span
              id="status"
              className={`${
                status === "Publish" ? "text-blue-600" : "text-red-600"
              }`}
            >
              {status}
            </span>
          </div>
          
          <div>
            <div dangerouslySetInnerHTML={{ __html: description }} />
          </div>
          <div className="mb-2 inline-flex">
            {tags.map((tag, index) => (
              <label className="bg-blue-300 m-1 rounded-md p-1" key={index}>
                <span className="font-bold">{tag}</span>
              </label>
            ))}
          </div>
        </div>
      </div>
    </div>
  </Layout>
  );
};


export default EditBlog;
