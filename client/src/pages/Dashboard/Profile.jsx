import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";

import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getMe } from "../../features/authSlice";

import { Helmet } from "react-helmet";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import { convertToHTML } from "draft-convert";
const Profile = () => {
  const [name, setName] = useState("");
  const [motto, setMotto] = useState("");
  const [role, setRole] = useState("");
  const [about, setAbout] = useState();
  const [location, setLocation] = useState("");
  const [email, setEmail] = useState("");
  const [contact, setContact] = useState("");
  const [file, setFile] = useState("");
  const [emailPass, setEmailPass] = useState("");
  const [preview, setPreview] = useState("");
  const { isError, user } = useSelector((state) => state.auth);

  const [msg, setMsg] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const handleEditorChange = (state) => {
    setEditorState(state);
    convertContentToHTML();
  };
  const convertContentToHTML = () => {
    let currentContentAsHTML = convertToHTML(editorState.getCurrentContent());
    setAbout(currentContentAsHTML);
  };
  useEffect(() => {
    const getProfile = async () => {
      const response = await axios.get("http://localhost:5000/profile");
      setName(response.data[0].description);
      setMotto(response.data[1].description);
      setRole(response.data[2].description);
      setAbout(response.data[3].description);
      setLocation(response.data[4].description);
      setEmail(response.data[5].description);
      setContact(response.data[6].description);
      setFile(response.data[7].description);
      setEmailPass(response.data[8].description);
    };
    getProfile();
  }, []);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);
  const loadImage = (e) => {
    const image = e.target.files[0];
    setFile(image);
    setPreview(URL.createObjectURL(image));
  };
  const updateProfile = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("name", name);
    formData.append("motto", motto);
    formData.append("role", role);
    formData.append("about", about);
    formData.append("location", location);
    formData.append("email", email);
    formData.append("contact", contact);
    formData.append("file", file);
    formData.append("emailPass", emailPass);

    try {
      await axios.patch(`http://localhost:5000/profile`, formData);
      navigate("/admin/profile");
      setMsg(`
      <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
   Profile Successfully Updated
      </div>
    `);
    } catch (error) {
      if (error.response) {
        setMsg(`
        <div className="w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium hover:border-red-500">
      ${error.response.data.msg}
        </div>
      `);
      }
    }
  };
  return (
    <Layout title="Profile">
        <div className="px-4">
        <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-2 mx-auto mt-24">
          <div className="relative p-4 flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-sm rounded ">
            <div dangerouslySetInnerHTML={{ __html: msg }} />
            {preview ? (
              <div className="grid place-items-center">
                <figure style={{ width: "400px" }}>
                  <img src={preview} alt="project" className="h-200" />
                </figure>
              </div>
            ) : (
              <div className="grid place-items-center">
                <figure style={{ width: "400px" }}>
                  <img src="/logo192.png" alt="project" className="h-200" />
                </figure>
              </div>
            )}

            <form onSubmit={updateProfile}>
              <div className="mb-5">
                <input
                  onChange={loadImage}
                  type="file"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Name"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Name
                </label>
                <input
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  type="text"
                  name="Name"
                  id="Name"
                  placeholder="Name"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Motto"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Motto
                </label>
                <input
                  value={motto}
                  onChange={(e) => setMotto(e.target.value)}
                  type="text"
                  name="Motto"
                  id="Motto"
                  placeholder="Motto"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Role"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Role
                </label>
                <input
                  value={role}
                  onChange={(e) => setRole(e.target.value)}
                  type="text"
                  name="Role"
                  id="Role"
                  placeholder="Role"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
             
              <div className="mb-5">
                <label
                  htmlFor="About"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  About
                </label>
                <Editor
                  editorState={editorState}
                  onEditorStateChange={handleEditorChange}
                  wrapperClassName="wrapper-class"
                  editorClassName="editor-class"
                  toolbarClassName="toolbar-class"
                  className="form-control"
                />
              </div>
                  <div className="mb-5">
                <label
                  htmlFor="Location"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Location
                </label>
                <input
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
                  type="text"
                  name="Location"
                  id="Location"
                  placeholder="Location"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
                  <div className="mb-5">
                <label
                  htmlFor="Email"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Email
                </label>
                <input
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  type="email"
                  name="Email"
                  id="Email"
                  placeholder="Email"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
                  <div className="mb-5">
                <label
                  htmlFor="EmailPass"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Email Password
                </label>
                <input
                  value={emailPass}
                  onChange={(e) => setEmailPass(e.target.value)}
                  type="text"
                  name="EmailPass"
                  id="EmailPass"
                  placeholder="EmailPass"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
              <div className="mb-5">
                <label
                  htmlFor="Contact"
                  className="mb-3 block text-base font-medium text-[#07074D]"
                >
                  Contact
                </label>
                <input
                  value={contact}
                  onChange={(e) => setContact(e.target.value)}
                  type="text"
                  name="Contact"
                  id="Contact"
                  placeholder="Contact"
                  className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-2 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                />
              </div>
            
              <div>
                <button
                  type="submit"
                  className="hover:shadow-form rounded-md bg-[#6A64F1] py-3 px-8 text-base font-semibold text-white outline-none"
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Profile;
