import io from "socket.io-client";
import Chat from "./Chat";
import { useNavigate } from "react-router-dom";
import { getMe } from "../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import Layout from "../../components/dashboard/Layout";
import axios from "axios";

const socket = io.connect("http://localhost:5000/");
const Message = () => {
  const [username, setUsername] = useState("");
  const [name, setName] = useState("");
  const [receiverName, setReceiverName] = useState("");
  const [room, setRoom] = useState("");
  const [showChat, setShowChat] = useState(false);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);
  const [users, setUsers] = useState([]);

  const currentRoom = async (userId) => {
    const userData = await axios.get(`http://localhost:5000/users/${userId}`);
    setName(user.name);
    setRoom(userData.data.uuid);
    setReceiverName(userData.data.name);
    socket.emit("join_room", userData.data.uuid);
    setShowChat(true);
  };

  useEffect(() => {
    const getUsers = async () => {
      const response = await axios.get("http://localhost:5000/users");
      setUsers(response.data);
    };
    getUsers();
  }, []);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if (user && user.role !== "admin") {
      navigate("/");
    }
  }, [isError, user, navigate]);

  return (
    <Layout title="Message">
      <div className="mt-3">
        <div className="container mx-auto">
          <div className="min-w-full border lg:grid lg:grid-cols-3">
            <div className="border-r border-gray-300 lg:col-span-1">
              <div className="mx-3 my-3">
                <div className="relative text-gray-600">
                  <span className="absolute inset-y-0 left-0 flex items-center pl-2">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      viewBox="0 0 24 24"
                      className="w-6 h-6 text-gray-300"
                    >
                      <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                  </span>
                  <input
                    type="search"
                    className="block w-full py-2 pl-10 bg-gray-100 rounded outline-none"
                    name="search"
                    placeholder="Search"
                    required
                  />
                </div>
              </div>
              <ul className="overflow-auto h-[32rem]">
                <h2 className="my-2 mb-2 ml-2 text-lg text-gray-600">Chats</h2>
                <li>
                  {users.map((user, index) => (
                    <div
                      key={index}
                      onClick={() => currentRoom(user.uuid)}
                      className={`${
                        room === user.uuid ? "bg-blue-200" : ""
                      } flex items-center px-3 py-2 text-sm transition duration-150 ease-in-out border-b border-gray-300 cursor-pointer hover:bg-gray-100 focus:outline-none`}
                    >
                      <div className="w-full pb-2">
                        <div className="flex justify-between">
                          <span className="block ml-2 font-semibold text-gray-600">
                            {user.name}
                          </span>
                          <span className="block ml-2 text-sm text-gray-600">
                            25 minutes
                          </span>
                        </div>
                      </div>
                    </div>
                  ))}
                </li>
              </ul>
            </div>
            <div className="hidden lg:col-span-2 lg:block">
              {showChat ? (
                <Chat socket={socket} username={username} room={room} receiverName={receiverName} />
              ) : (
                <div className="w-full">No Message</div>
              )}
            </div>
          </div>
        </div>
      </div>

     
    </Layout>
  );
};

export default Message;
