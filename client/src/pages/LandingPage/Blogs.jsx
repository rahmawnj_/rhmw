import React, { useEffect, useState } from "react";
import Layout from "../../components/landingpage/Layout";
import axios from "axios";
import { Link } from "react-router-dom";

const Blogs = () => {
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    async function getBlogs() {
      const response = await axios.get("http://localhost:5000/blogs");
      setBlogs(response.data);
    }
    getBlogs();
  }, []);

  return (
    <Layout title="Blogs">
      <section>
        {blogs.length !== 0 ? (
          blogs.map((blog, index) => {
            return (
              blog.status === "Publish" && (
                <div className="flex rounded-md overflow-hidden justify-center my-2" key={index}>
                  <div className="block hover:bg-gray-200 bg-sky-100 w-full ">
                    <img
                      src={`http://localhost:5000/images/blogs/${blog.image}`}
                      alt={blog.title}
                    />

                    <div className="p-6 text-center">
                      <h5 className="text-gray-900 text-xl font-medium mb-2">
                        {blog.title}
                      </h5>

                      <div className=" text-base justify-center flex flex-wrap w-full ">
                        {blog.tags.split(",").map((tag, index) => (
                          <span
                            key={index}
                            className="px-2 py-1 font-bold bg-purple-400 text-white rounded-lg hover:bg-gray-500 m-1"
                          >
                            {tag}
                          </span>
                        ))}
                      </div>
                    </div>

                    <div className="py-1 justify-between flex px-6 border-t bg-purple-300 border-gray-300 text-gray-600">
                      {blog.createdAt}
                      <Link
                        to={`/blogs/detail/${blog.uuid}`}
                        className="inline-block py-2 px-3 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                      >
                        <i className=" fa fa-arrow-right " />
                      </Link>
                    </div>
                  </div>
                </div>
              )
            );
          })
        ) : (
          <>No blog Found</>
        )}
      </section>
    </Layout>
  );
};

export default Blogs;
