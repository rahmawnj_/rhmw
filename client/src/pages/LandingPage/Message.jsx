import { useEffect, useState } from "react";
import React from "react";
import io from "socket.io-client";
import { getMe } from "../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const socket = io.connect("http://localhost:5000/");
const Message = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [currentMessage, setCurrentMessage] = useState();
  const [messageList, setMessageList] = useState([]);
  // const [name, setName] = useState([]);
  const { isError, user } = useSelector((state) => state.auth);

  const sendMessage = async () => {
    if (currentMessage !== "") {
      const socketId = await axios.get(`http://localhost:5000/settings/1`);

      const messageData = {
        room: user.uuid,
        author: user.uuid,
        senderId: user.uuid,
        receiverId: socketId.data.description,
        message: currentMessage,
        time:
          new Date(Date.now()).getHours() +
          ":" +
          new Date(Date.now()).getMinutes(),
      };
      await socket.emit("send_message", messageData);
      setMessageList((list) => [...list, messageData]);
      setCurrentMessage('')
    }
  };

  useEffect(() => {
    socket.on("receive_message", (data) => {
      setMessageList((list) => [...list, data]);
    });
  }, []);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (user && user.uuid) {
      socket.emit("join_room", user.uuid);
      const getMessages = async () => {
        const response = await axios.get(
          `http://localhost:5000/messages/${user.uuid}`
        );
        response.data.forEach((data) => {
          const messageData = {
            room: data.room,
            author: data.senderId,
            message: data.message,
            time: data.time,
          };
          setMessageList((list) => [...list, messageData]);
        });
      };
      getMessages();
    }
  }, [user]);

  useEffect(() => {
    if (isError) {
      navigate("/auth/login");
    }
  }, [isError, navigate]);

  return (
    <section className="">
      <div className=" mx-auto ">
        <div className="w-screen rounded">
          <div>
            <div className=" w-full">
              <div className="fixed w-full top-0 z-50 bg-blue-300 flex items-center p-3 border-b border-gray-300">
              
                <span className="block ml-10 font-bold text-gray-600">
                  Rahma
                </span>
                <span className="absolute w-3 h-3 bg-green-600 rounded-full left-10 top-3"></span>
              </div>
              <div className=" mt-20">
                <div className="static top-5 w-full p-6 overflow-y-auto ">
                  <ul className="space-y-2">
                  
                    {messageList.map((messageContent, index) => {
                      return (
                        <li
                          key={index}
                          className={
                            user.uuid !== messageContent.author
                              ? "flex justify-start"
                              : "flex justify-end"
                          }
                        >
                          <div className="relative max-w-xl px-4 py-2 text-gray-700 rounded shadow">
                            <span className="block">
                              {messageContent.message}
                            </span>
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>

              <div className="flex text-center content-center bg-blue-100 z-10 fixed bottom-0 items-center justify-between w-full p-3 border-t border-gray-300">
                <input
                  type="text"
                  value={currentMessage}
                  onChange={(event) => {
                    setCurrentMessage(event.target.value);
                  }}
                  onKeyPress={(event) => {
                    event.key === "Enter" && sendMessage();
                  }}
                  placeholder="Message"
                  className=" w-full py-2 pl-4 align-middle mx-3 bg-gray-100 rounded-full outline-none focus:text-gray-700"
                  name="message"
                  required
                />

                <button
                  type="submit"
                  className="hover:shadow-form bg-[#6A64F1] py-2 px-2 rounded-md text-center text-base font-semibold text-white outline-none"
                  onClick={sendMessage}
                >
                  <svg
                    className="w-5 h-5 text-white origin-center transform rotate-90"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
                  </svg>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Message;
