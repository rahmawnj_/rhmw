import React, { useEffect, useState } from "react";
import Layout from "../../components/landingpage/Layout";
import { useDispatch } from "react-redux";
import { getMe } from "../../features/authSlice";
import axios from "axios";
import { Link } from "react-router-dom";

const Home = () => {
  const [name, setName] = useState("");
  const [motto, setMotto] = useState("");
  const [about, setAbout] = useState("");
  const [file, setFile] = useState("");
  const dispatch = useDispatch();
  // const [skills, setSkills] = useState([]);
  const [socialmedias, setSocialMedias] = useState([]);

  useEffect(() => {
    const getProfile = async () => {
      const response = await axios.get("http://localhost:5000/profile");
      setName(response.data[0].description);
      setMotto(response.data[1].description);
      setAbout(response.data[3].description);
      setFile(response.data[7].description);
    };
    getProfile();
    const getSkills = async () => {
      // const response = await axios.get("http://localhost:5000/skills");
      // setSkills(response.data);
    };
    getSkills();
    const getSocialMedias = async () => {
      const response = await axios.get("http://localhost:5000/socialmedias");
      setSocialMedias(response.data);
    };
    getSocialMedias();
  }, []);
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);
  return (
    <Layout title="Home">
      <section className="my-3">
        <div className="lg:px-24 md:px-44 px-4 items-center flex justify-center flex-col-reverse lg:flex-row md:gap-28 gap-16">
          <div className=" w-full xl:w-1/2 pb-12 lg:pb-0">
            <div className="flex items-center justify-center bg-white">
              <div className="mx-auto max-w-[43rem]">
                <div className="text-center">
                  <h3 className="mt-3 text-[2rem] leading-[4rem] tracking-tight text-black">
                    {name}
                  </h3>
                  <h3 className="my-3  title text-[2rem] font-bold leading-relaxed text-slate-400">
                    <b className="title-word title-word">
                      <u>{motto}</u>
                    </b>
                  </h3>

                  <div className="w-full flex justify-center">
                    {socialmedias &&
                      socialmedias.map((socialmedia, index) => {
                        return (
                          socialmedia.status === "Publish" && (
                            <div
                              title={socialmedia.name}
                              key={index}
                              className="p-2 mx-2 font-semibold text-white hover:text-black bg-pink-400 hover:bg-purple-400  hover:border-2 border-2 border-pink-400 inline-flex items-center space-x-2 rounded-full"
                            >
                              <i
                                className={`fa-brands  ${socialmedia.icon}`}
                              ></i>
                            </div>
                          )
                        );
                      })}
                  </div>
                </div>
                <div className="mt-6 flex items-center justify-center gap-4">
                  <Link
                    to="#"
                    className=" rounded-md bg-indigo-600/95 px-5 py-3 font-medium text-white hover:bg-indigo-500"
                  >
                    Download Resume
                  </Link>
                  <Link
                    to="/projects"
                    className=" rounded-md border border-slate-200 px-5 py-3 font-medium text-slate-900 hover:bg-slate-100"
                  >
                    Projects
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div>
            {file && (
              <img
                src={`http://localhost:5000/images/profile/${file}`}
                width={700}
                alt={name}
              />
            )}
          </div>
        </div>
        <div className="p-20 space-y-8">
          <h1 className="text-4xl text-center my-20">About</h1>
          <div
            className="text-center"
            dangerouslySetInnerHTML={{ __html: about }}
          />
        </div>
      </section>
    </Layout>
  );
};

export default Home;
