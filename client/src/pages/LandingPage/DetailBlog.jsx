import React, { useEffect, useState } from "react";
import Layout from "../../components/landingpage/Layout";
import axios from "axios";
import { useParams } from "react-router-dom";

const BlogsDetail = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [tags, setTags] = useState([]);
  const [image, setImage] = useState("");
  const [msg, setMsg] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const getBlogById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/blogs/${id}`);
        setTitle(response.data.title);
        setDescription(response.data.description);
        const Tags = response.data.tags;
        const newTags = Tags.split(",");
        setTags(newTags);
        setImage(response.data.image);
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getBlogById();
  }, [id]);

  return (
    <Layout title="Blog">
      <section className="mt-10">
      {msg}

        <div className="mb-4 md:mb-0 w-full max-w-screen-md mx-auto relative">
          <div className="absolute left-0 bottom-0 w-full h-full z-10" />
          {image && (
            <img
              src={`http://localhost:5000/images/blogs/${image}`}
              alt="blog"
            />
          )}
        </div>
        <div className="px-4 lg:px-0 mt-12 text-gray-700 max-w-screen-md mx-auto text-lg leading-relaxed">
          <div className="text-center">{title}</div>
          <div
            className="text-center"
            dangerouslySetInnerHTML={{ __html: description }}
          />
          <div className="text-base justify-center flex flex-wrap  w-full ">
            {tags.map((tag, index) => (
              <span
                key={index}
                className="px-2 py-1 font-bold bg-purple-400 text-white rounded-lg hover:bg-gray-500 m-1"
              >
                {tag}
              </span>
            ))}
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default BlogsDetail;
