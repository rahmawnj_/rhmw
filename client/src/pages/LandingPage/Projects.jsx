import React, { useEffect, useState } from "react";
import Layout from "../../components/landingpage/Layout";
import axios from "axios";
import { Link } from "react-router-dom";

const Projects = () => {
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    async function getProjects() {
      const response = await axios.get("http://localhost:5000/projects");
      setProjects(response.data);
    }
    getProjects();
  }, []);

  return (
    <Layout title={'Projects'}>

      <section>
        {projects.length !== 0 ? (
          projects.map((project, index) => {
            return (
              project.status === "Publish" && (
                <div className="flex rounded-md overflow-hidden justify-center my-2" key={index}>
                  <div className="block  hover:bg-gray-200 bg-sky-100 w-full ">
                      <img
                        src={`http://localhost:5000/images/projects/${project.image}`}
                        alt={project.name}
                      />

                    <div className="p-6 text-center">
                      <h5 className="text-gray-900 text-xl font-medium mb-2">
                        {project.name}
                      </h5>

                      <div className=" text-base justify-center flex flex-wrap w-full ">
                        {project.tags.split(",").map((tag, index) => (
                          <span
                            key={index}
                            className="px-2 py-1 font-bold bg-purple-400 text-white  hover:bg-gray-500 m-1"
                          >
                            {tag}
                          </span>
                        ))}
                      </div>
                    </div>

                    <div className="py-3  text-right px-6 border-t bg-purple-300 border-gray-300 text-gray-600">
                      <Link
                        to={`/projects/detail/${project.uuid}`}
                        className="inline-block py-2 px-3 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                      >
                        <i className=" fa fa-arrow-right " />
                      </Link>
                    </div>
                  </div>
                </div>
              )
            );
          })
        ) : (
          <>No Project Found</>
        )}
      </section>
    </Layout>
  );
};


export default Projects;
