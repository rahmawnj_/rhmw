import React from "react";
import Layout from "../../components/landingpage/Layout";
import { Link } from "react-router-dom";

const PageNotFound = () => {
 
  return (
    <Layout title="Page Not Found">
  {/* component */}
<div className="lg:px-24 lg:py-24 md:py-20 md:px-44 px-4 py-24 items-center flex justify-center flex-col-reverse lg:flex-row md:gap-28 gap-16">
  <div className="xl:pt-24 w-full xl:w-1/2 relative pb-12 lg:pb-0">
    <div className="relative">
      <div className="absolute">
        <div className>
          <h1 className="my-2 text-gray-800 font-bold text-2xl">
            Uhmm! Looks like you've found the
            doorway to the great nothing
          </h1>
          <div>
          <Link to="/home" className="sm:w-full lg:w-auto my-2 border rounded md p-2 text-center bg-indigo-600 text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-700 focus:ring-opacity-50">Take me there!</Link>
          </div>
        </div>
      </div>
      <div>
        <img src="/404-2.png" alt="Page not Found" />
      </div>
    </div>
  </div>
  <div>
    <img src="/404.png" alt="Page not Found" />
  </div>
</div>

    </Layout>
  );
};

export default PageNotFound;
