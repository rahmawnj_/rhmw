import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { reset } from "../../features/authSlice";
import { getMe } from "../../features/authSlice";
import Layout from "../../components/landingpage/Layout";
import axios from "axios";

const Registration = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confPassword, setConfPassword] = useState("");
  const [msg, setMsg] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isError, user } = useSelector((state) => state.auth);


  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (user) {
      if (user.role === "admin") {
        navigate("/admin/dashboard");
      } else {
        navigate("/");
      }
    }
    dispatch(reset());
  }, [user, dispatch, navigate]);

  const SignUp = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/registration", {
        name: name,
        email: email,
        password: password,
        confPassword: confPassword,
      });
      navigate("/auth/login");
    } catch (error) {
      if (error.response) {
        setMsg(`
        <div className="hover:red-yellow-500 w-full mb-2 select-none border-l-4 border-red-400 bg-red-100 p-4 font-medium">
      ${error.response.data.msg}
        </div>
      `);
      }
    }
  };
  return (
    <Layout>
<div className="bg-gray-100 flex flex-col justify-center">
  <div className="mx-auto md:w-full md:max-w-md">
    <div className="bg-white shadow w-full rounded-lg divide-y divide-gray-200">
      <div className="px-5 py-7">
    <h1 className="font-bold text-center text-2xl mb-5">RHM</h1>  
    <form onSubmit={SignUp}>
    <div dangerouslySetInnerHTML={{ __html: msg }} />
        <label className="font-semibold text-sm text-gray-600 pb-1 block">Name</label>
        <input required type="text" className="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full" value={name} onChange={(e) => setName(e.target.value)} />
        <label className="font-semibold text-sm text-gray-600 pb-1 block">E-mail</label>
        <input required type="email" className="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full" value={email} onChange={(e) => setEmail(e.target.value)} />
        <label className="font-semibold text-sm text-gray-600 pb-1 block">Password</label>
        <input required type="password" className="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full" value={password} onChange={(e) => setPassword(e.target.value)}/>
        <label className="font-semibold text-sm text-gray-600 pb-1 block">Password Confirm</label>
        <input required type="password" className="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full" value={confPassword} onChange={(e) => setConfPassword(e.target.value)}/>
        <button type="submit" className="transition duration-200 bg-blue-500 hover:bg-blue-600 focus:bg-blue-700 focus:shadow-sm focus:ring-4 focus:ring-blue-500 focus:ring-opacity-50 text-white w-full py-2.5 rounded-lg text-sm shadow-sm hover:shadow-md font-semibold text-center inline-block">
          <span className="inline-block mr-2">Registration</span>
        </button>
    </form>

      </div>
      <div className="py-1">
        <div className="grid grid-cols-2 gap-1">
          <div className="text-center sm:text-right  whitespace-nowrap">
            <button className="transition duration-200 mx-5 px-5 py-4 cursor-pointer font-normal text-sm rounded-lg text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-200 focus:ring-2 focus:ring-gray-400 focus:ring-opacity-50 ring-inset">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-4 h-4 inline-block align-text-bottom	">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M18.364 5.636l-3.536 3.536m0 5.656l3.536 3.536M9.172 9.172L5.636 5.636m3.536 9.192l-3.536 3.536M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-5 0a4 4 0 11-8 0 4 4 0 018 0z" />
              </svg>
              <span className="inline-block ml-1">Have an Account?</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    </Layout>

  );
};

export default Registration;
