import React, { useEffect, useState } from "react";
import Layout from "../../components/landingpage/Layout";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import {FaMapMarkerAlt, FaPhoneAlt, FaEnvelope} from 'react-icons/fa'
const Home = () => {
  const [location, setLocation] = useState("");
  const [email, setEmail] = useState("");
  const [contact, setContact] = useState("");
  const [senderName, setSenderName] = useState("");
  const [senderEmail, setSenderEmail] = useState("");
  const [senderMessage, setSenderMessage] = useState("");
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    const getProfile = async () => {
      const response = await axios.get("http://localhost:5000/profile");
      setLocation(response.data[4].description);
      setEmail(response.data[5].description);
      setContact(response.data[6].description);
    };
    getProfile();
  }, []);
  const sendMail = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/mails", {
        name: senderName,
        email: senderEmail,
        message: senderMessage,
      });
      navigate("/contact");
      setSenderName("");
      setSenderEmail("");
      setSenderMessage("");
      setMsg(`
      <div class="w-full mb-2 select-none border-l-4 border-blue-400 bg-blue-100 p-4 font-medium hover:border-blue-500">Email has been sent. Thankyou!</div>
    `);
    } catch (error) {
      if (error.response) {
        setMsg(`
      <div class="w-full mb-2 select-none border-l-4 border-blue-400 bg-blue-100 p-4 font-medium hover:border-blue-500">
     ${error.response.data.msg}
      !</div>
      
      `);
      }
    }
  };

  return (
    <Layout title="Contact">
      
      <section className="">
        <div className="rounded-md flex items-top max-w-screen-lg justify-center bg-purple-300 sm:items-center sm:pt-0">
          <div className="mx-auto ">
            <div className="overflow-hidden">
              <div className="grid grid-cols-1 md:grid-cols-2">
                <div className="p-6 mr-2 ">
                  <h1 className="text-4xl mb-4 sm:text-5xl text-black font-extrabold tracking-tight">
                    Contact
                  </h1>
                  <div className="flex items-center my-2 text-black">
                    <div className="h-10 w-10 text-center grid content-center items-center justify-center bg-white rounded-full">
                    <FaMapMarkerAlt />
                    </div>
                    <div className="ml-4 text-md tracking-wide font-semibold">
                      {location}
                    </div>
                  </div>
                  <div className="flex items-center my-2 text-black">
                  <div className="h-10 w-10 text-center grid content-center items-center justify-center bg-white rounded-full">
                    <FaPhoneAlt />
                    </div>
                    <div className="ml-4 text-md tracking-wide font-semibold">
                      {contact}
                    </div>
                  </div>
                  <div className="flex items-center my-2 text-black">
                  <div className="h-10 w-10 text-center grid content-center items-center justify-center bg-white rounded-full">
                    <FaEnvelope />
                    </div>
                    <div className="ml-4 text-md tracking-wide font-semibold">
                      {email}
                    </div>
                  </div>
                </div>
                <div className="p-6 w-full flex flex-col justify-center">
                  <div dangerouslySetInnerHTML={{ __html: msg }} />

                  <form onSubmit={sendMail} className="">
                    <div className="-mx-3 flex flex-wrap">
                      <div className="w-full px-3 sm:w-1/2">
                        <div className="mb-5">
                          <label
                            htmlFor="fName"
                            className="mb-3 block text-base font-medium text-[#07074D]"
                          >
                            Name
                          </label>
                          <input
                            type="text"
                            name="name"
                            value={senderName}
                            onChange={(e) => setSenderName(e.target.value)}
                            required
                            placeholder="Name"
                            className="w-full rounded-sm border border-blue-400 bg-white py-1 px-1 text-base font-medium text-[#6B7280] outline-none focus:ring focus:border-blue-500"
                          />
                        </div>
                      </div>
                      <div className="w-full px-3 sm:w-1/2">
                        <div className="mb-5">
                          <label
                            htmlFor="email"
                            className="mb-3 block text-base font-medium text-[#07074D]"
                          >
                            Email
                          </label>
                          <input
                            name="email"
                            value={senderEmail}
                            onChange={(e) => setSenderEmail(e.target.value)}
                            type="email"
                            required
                            placeholder="Email"
                            className="w-full rounded-sm border border-blue-400 bg-white py-1 px-1 text-base font-medium text-[#6B7280] outline-none focus:ring focus:border-blue-500"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="mb-5">
                      <label
                        htmlFor="guest"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        Message
                      </label>
                      <textarea
                        name="message"
                        value={senderMessage}
                        onChange={(e) => setSenderMessage(e.target.value)}
                        rows={6}
                        className="w-full appearance-none rounded-sm border border-blue-400 bg-white py-1 px-1 text-base font-medium text-[#6B7280] outline-none focus:ring focus:border-blue-500"
                      ></textarea>
                    </div>
                    <div className="">
                      <button className="hover:shadow-form rounded-sm bg-[#6A64F1] py-2 px-8 text-center text-base font-semibold text-white outline-none">
                        Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </Layout>
  );
};

export default Home;
