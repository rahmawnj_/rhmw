import React, { useState } from "react";
import { NavLink, useNavigate, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { LogOut, reset } from "../../features/authSlice";

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { user } = useSelector((state) => state.auth);
  let [open, setOpen] = useState(false);

  const logout = () => {
    dispatch(LogOut());
    dispatch(reset());
    navigate("/");
  };

  let Links = [
    { name: "HOME", link: "/home" },
    { name: "PROJECTS", link: "/projects" },
    { name: "BLOGS", link: "/blogs" },
    { name: "CONTACT", link: "/contact" },
  ];

  return (
    <header className="shadow-md w-11/12 z-50 rounded-md mx-auto fixed top-5">
      <div className="md:flex items-center rounded-md justify-between bg-sky-200 py-4 md:px-10 px-7">
        <div
          className="font-bold text-2xl cursor-pointer flex items-center font-mono 
text-gray-800"
        >
          <span className="text-3xl text-indigo-600 mr-1 pt-2">RHM</span>
        </div>

        <div
          onClick={() => setOpen(!open)}
          className="text-3xl absolute right-8 top-6 cursor-pointer md:hidden"
        >
          {open ? "x" : "="}
        </div>

        <ul
          className={`md:flex md:items-center md:pb-0 pb-12 absolute md:static bg-sky-200 md:z-auto z-[-1] left-0 w-full md:w-auto md:pl-0 pl-9  ${
            open ? "top-20 " : "hidden"
          }`}
        >
          {Links.map((link) => (
            <li key={link.name} className="md:mx-4 text-md md:my-0 my-7">
              <NavLink
                to={link.link}
                className={({isActive}) => (isActive ? 'hover:text-black-600 text-black font-bold underline decoration-pink-500' : 'hover:text-black-600 hover:underline hover:decoration-yellow-300 text-black')}
              >
                {link.name}
              </NavLink>
            </li>
          ))}

          {user ? (
            <div className="grid place-content-center">
              <div className="inline">
                <span className="font-bold mx-2">{user.name}</span>
                <button
                  onClick={logout}
                  className=" w-30 uppercase mx-auto shadow bg-red-600 hover:bg-indigo-700 focus:shadow-outline focus:outline-none text-white text-xs py-3 px-10 rounded"
                >
                  Sign Out
                </button>
              </div>
            </div>
          ) : (
            <div className="grid place-content-center">
              <div className="flex items-center align-middle ">
              <Link
                to={"/auth/login"}
                className="mx-1 w-30 uppercase shadow bg-indigo-800 hover:text-white hover:bg-indigo-700 focus:shadow-outline focus:outline-none text-white text-xs py-3 px-10 rounded"
              >
                Sign in
              </Link>
              <Link
                to={"/auth/registration"}
                className="mx-1 w-30 uppercase shadow bg-slate-500 hover:text-white hover:bg-slate-700 focus:shadow-outline focus:outline-none text-white text-xs py-3 px-10 rounded"
              >
                Sign Up
              </Link>
              </div>
            </div>
          )}
        </ul>
      </div>
    </header>
  );
};

export default Header;
