import "../../index.css";
import { Fragment } from "react";
import Header from "./Header";
import Footer from "./Footer";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { FaCommentDots } from 'react-icons/fa';
function Layout({ children, title }) {
  return (
    <Fragment>
      <Helmet>
        <title>{`RHM | ${title ? title : ''}`}</title>
      </Helmet>
      <div className="grid place-items-center ">
        <Header />
        <main className="mt-32 lg:mt-32 p-6">
          {children}
          <Link
            to={"/message"}
            className="fixed z-90 bottom-10 right-8 bg-blue-600 w-20 h-20 rounded-full drop-shadow-lg flex justify-center items-center text-white text-4xl hover:bg-blue-700 hover:drop-shadow-2xl hover:animate-bounce duration-300"
          >
            <FaCommentDots />
          </Link>
        </main>
        <Footer />
      </div>
    </Fragment>
  );
}

export default Layout;
