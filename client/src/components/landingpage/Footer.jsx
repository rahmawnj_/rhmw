import React, { useEffect, useState } from "react";
import axios from "axios";
const Footer = () => {
  const [name, setName] = useState("");
  const [motto, setMotto] = useState("");
  const [socialmedias, setSocialMedias] = useState([]);
  useEffect(() => {
    const getProfile = async () => {
      const response = await axios.get("http://localhost:5000/profile");
      setName(response.data[0].description);
      setMotto(response.data[1].description);
    };
    getProfile();
    const getSocialMedias = async () => {
      const response = await axios.get("http://localhost:5000/socialmedias");
      setSocialMedias(response.data);
    };
    getSocialMedias();
  }, []);

  return (
 
    <footer className=" bg-purple-200 w-full pt-8 pb-6">
      <div className="container mx-auto px-4">
        <div className="flex mt-4 flex-wrap items-center md:justify-between justify-center">
          <div className="w-full md:w-4/12 px-4 mx-auto text-center">
            {socialmedias &&
              socialmedias.map((socialmedia, index) => {
                return (
                  socialmedia.status === "Publish" && (
                    <button key={index} className=" p-2 mx-2 font-semibold text-white bg-pink-400 hover:bg-purple-400 hover:text-pink-400 hover:border-2 border-2 border-pink-400 inline-flex items-center space-x-2 rounded-full">
                      <i className={`fa-brands ${socialmedia.icon}`}></i>
                    </button>
                  )
                );
              })}

            <div className="text-sm mt-6 text-blueGray-500 font-semibold py-1">
              {name} - {motto}
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
