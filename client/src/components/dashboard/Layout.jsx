import React, { useState, Fragment } from "react";
import Headbar from "./Headbar";
import Sidebar from "./Sidebar";
import Footbar from "./Footbar";
import { Helmet } from "react-helmet";
const Layout = ({ children, title }) => {
  const [isOpened, setIsOpened] = useState(false);

  return (
    <Fragment>
      <Helmet>
        <title>{`RHM | ${title ? title : ""}`}</title>
      </Helmet>
      <Headbar statusSidebar={isOpened} setStatusSidebar={setIsOpened} />
      <div className="flex overflow-hidden bg-white pt-16">
        <Sidebar sidebarStatus={isOpened} title={title} />
        <div
          className="bg-gray-900 opacity-50 hidden fixed inset-0 z-10"
          id="sidebarBackdrop"
        />
        <div
          id="main-content"
          className="h-full w-full bg-gray-50 relative overflow-y-auto lg:ml-64"
        >
          <main>
        {children}
          </main>
        <Footbar />
        </div>
      </div>
    </Fragment>
  );
};

export default Layout;
