import { NavLink } from "react-router-dom";
import {
  FaTachometerAlt,
  FaUserCircle,
  FaRocketchat,
  FaBrain,
  FaLink,
  FaUser,
  FaToolbox,
  FaNewspaper,
} from "react-icons/fa";
// import { useDispatch, useSelector } from "react-redux";
// import { LogOut, reset } from "../../features/authSlice";

const Sidebar = ({ sidebarStatus, title }) => {
  // const dispatch = useDispatch();
  // const navigate = useNavigate();
  // const { user } = useSelector((state) => state.auth);

  // const logout = () => {
  //   dispatch(LogOut());
  //   dispatch(reset());
  //   navigate("/");
  // };

  return (
    <aside
      id="sidebar"
      className={`fixed z-20 h-full top-0 left-0 pt-16 flex lg:flex flex-shrink-0 flex-col w-64 transition-width duration-75 ${
        sidebarStatus ? "" : "hidden"
      }`}
      aria-label="Sidebar"
    >
      <div className="relative flex-1 flex flex-col min-h-0 border-r border-gray-200 bg-white pt-0">
        <div className="flex-1 flex flex-col pt-5 pb-4 overflow-y-auto">
          <div className="flex-1 px-3 bg-white divide-y space-y-1">
            <ul className="space-y-2 pb-2">
              <li>
                <form action="#" method="GET" className="lg:hidden">
                  <label htmlFor="mobile-search" className="sr-only">
                    Search
                  </label>
                  <div className="relative">
                    <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                      <svg
                        className="w-5 h-5 text-gray-500"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
                      </svg>
                    </div>
                    <input
                      type="text"
                      name="email"
                      id="mobile-search"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-cyan-600  block w-full pl-10 p-2.5"
                      placeholder="Search"
                    />
                  </div>
                </form>
              </li>

              <li>
                <NavLink
                  to="/admin/dashboard"
                  className={`${title === "Dashboard" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}
                >
                  <FaTachometerAlt className="ml-1" />
                  <span className="ml-3">Dashboard</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/profile"
                  className={`${title === "Profile" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}
                >
                  <FaUserCircle className="ml-1" />

                  <span className="ml-3 flex-1 whitespace-nowrap">Profile</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/message"
                  className={`${title === "Message" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}

                >
                  <FaRocketchat className="ml-1" />
                  <span className="ml-3 flex-1 whitespace-nowrap">Message</span>
                  <span className="bg-gray-200 text-gray-800 ml-3 text-sm font-medium inline-flex items-center justify-center px-2 rounded-full">
                    Pro
                  </span>
                </NavLink>
              </li>
              <li>
                <hr />
              </li>
              <li>
                <NavLink
                  to="/admin/skills"
                  className={`${title === "Skill" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}

                >
                  <FaBrain className="ml-1" />
                  <span className="ml-3 flex-1 whitespace-nowrap">Skills</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/socialmedias"
                  className={`${title === "Social Media" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}

                >
                  <FaLink className="ml-1" />
                  <span className="ml-3 flex-1 whitespace-nowrap">
                    Social Medias
                  </span>
                </NavLink>
              </li>

              <li>
                <NavLink
                  to="/admin/users"
                  className={`${title === "User" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}

                >
                  <FaUser className="ml-1" />

                  <span className="ml-3 flex-1 whitespace-nowrap">Users</span>
                 
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/projects"
                  className={`${title === "Project" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}

                >
                  <FaToolbox className="ml-1" />
                  <span className="ml-3 flex-1 whitespace-nowrap">
                    Projects
                  </span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/blogs"
                  className={`${title === "Blog" && 'bg-blue-100'} text-base text-gray-900 font-normal flex items-center p-2 hover:bg-gray-100 group`}

                >
                  <FaNewspaper className="ml-1" />

                  <span className="ml-3 flex-1 whitespace-nowrap">Blogs</span>
                </NavLink>
              </li>
            </ul>
          
          </div>
        </div>
      </div>
    </aside>
  );
};

export default Sidebar;
