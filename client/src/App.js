import { BrowserRouter, Routes, Route } from "react-router-dom";
import LandingPageHome from "./pages/LandingPage/Home";
import LandingPageContact from "./pages/LandingPage/Contact";
import LandingPageProjects from "./pages/LandingPage/Projects";
import LandingPageBlogs from "./pages/LandingPage/Blogs";
import LandingPageDetailProject from "./pages/LandingPage/DetailProject";
import LandingPageDetailBlog from "./pages/LandingPage/DetailBlog";
import LandingPageMessage from "./pages/LandingPage/Message";
import Login from "./pages/LandingPage/Login";
import Registration from "./pages/LandingPage/Registration";
import PageNotFound from "./pages/LandingPage/PageNotFound";
import Dashboard from "./pages/Dashboard/Dashboard";
import DashboardProfile from "./pages/Dashboard/Profile";
import DashboardSettings from "./pages/Dashboard/Settings";
import DashboardUsers from "./pages/Dashboard/Users";
import DashboardAddUser from "./pages/Dashboard/AddUser";
import DashboardEditUser from "./pages/Dashboard/EditUser";
import DashboardDetailUser from "./pages/Dashboard/DetailUser";
import DashboardBlogs from "./pages/Dashboard/Blogs";
import DashboardAddBlog from "./pages/Dashboard/AddBlog";
import DashboardEditBlog from "./pages/Dashboard/EditBlog";
import DashboardDetailBlog from "./pages/Dashboard/DetailBlog";
import DashboardProjects from "./pages/Dashboard/Projects";
import DashboardAddProject from "./pages/Dashboard/AddProject";
import DashboardEditProject from "./pages/Dashboard/EditProject";
import DashboardDetailProject from "./pages/Dashboard/DetailProject";
import DashboardSocialMedias from "./pages/Dashboard/SocialMedias";
import DashboardAddSocialMedia from "./pages/Dashboard/AddSocialMedia";
import DashboardEditSocialMedia from "./pages/Dashboard/EditSocialMedia";
import DashboardDetailSocialMedia from "./pages/Dashboard/DetailSocialMedia";
import DashboardSkills from "./pages/Dashboard/Skills";
import DashboardAddSkill from "./pages/Dashboard/AddSkill";
import DashboardEditSkill from "./pages/Dashboard/EditSkill";
import DashboardDetailSkill from "./pages/Dashboard/DetailSkill";
import DashboardMessage from "./pages/Dashboard/Message";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPageHome />} />
        <Route path="/auth/login" element={<Login />} />
        <Route path="/auth/registration" element={<Registration />} />
        <Route path="/home" element={<LandingPageHome />} />
        <Route path="/contact" element={<LandingPageContact />} />
        <Route path="/message" element={<LandingPageMessage />} />
        <Route path="/projects" element={<LandingPageProjects />} />
        <Route path="/projects/detail/:id" element={<LandingPageDetailProject />} />
        <Route path="/blogs" element={<LandingPageBlogs />} />
        <Route path="/blogs/detail/:id" element={<LandingPageDetailBlog />} />
        <Route path="/admin/dashboard" element={<Dashboard />} />
        <Route path="/admin/message" element={<DashboardMessage />} />
        <Route path="/admin/profile" element={<DashboardProfile />} />
        <Route path="/admin/settings" element={<DashboardSettings />} />
        <Route path="/admin/users" element={<DashboardUsers />} />
        <Route path="/admin/users/add" element={<DashboardAddUser />} />
        <Route path="/admin/users/edit/:id" element={<DashboardEditUser />} />
        <Route path="/admin/users/detail/:id" element={<DashboardDetailUser />} />
        <Route path="/admin/projects" element={<DashboardProjects />} />
        <Route path="/admin/projects/add" element={<DashboardAddProject />} />
        <Route path="/admin/projects/edit/:id" element={<DashboardEditProject />} />
        <Route path="/admin/projects/detail/:id" element={<DashboardDetailProject />} />
        <Route path="/admin/blogs" element={<DashboardBlogs />} />
        <Route path="/admin/blogs/add" element={<DashboardAddBlog />} />
        <Route path="/admin/blogs/edit/:id" element={<DashboardEditBlog />} />
        <Route path="/admin/blogs/detail/:id" element={<DashboardDetailBlog />} />
        <Route path="/admin/socialmedias" element={<DashboardSocialMedias />} />
        <Route path="/admin/socialmedias/add" element={<DashboardAddSocialMedia />} />
        <Route path="/admin/socialmedias/edit/:id" element={<DashboardEditSocialMedia />} />
        <Route path="/admin/socialmedias/detail/:id" element={<DashboardDetailSocialMedia />} />
        <Route path="/admin/skills" element={<DashboardSkills />} />
        <Route path="/admin/skills/add" element={<DashboardAddSkill />} />
        <Route path="/admin/skills/edit/:id" element={<DashboardEditSkill />} />
        <Route path="/admin/skills/detail/:id" element={<DashboardDetailSkill />} />

        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
