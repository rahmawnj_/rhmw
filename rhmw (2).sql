-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2022 at 01:51 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rhmw`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `uuid`, `title`, `description`, `status`, `tags`, `image`, `createdAt`, `updatedAt`) VALUES
(10, '696218c1-18d1-43aa-bd91-927cf6c74ab5', 'fdfdfdsfds', '<p>dkaujsdgjd</p>', 'Publish', 'dfssdfffd', '513157edc7407a0d48ef6680f9c5f2c5.jpg', '2022-10-16 09:20:53', '2022-10-16 09:24:11'),
(11, 'c6185d39-8a7b-49ac-930b-e5c6d8bd8a70', 'fdgdgffd', 'null', 'Draft', 'fsdfds', 'deef505932c61f7a5a16db1748eb0d48.png', '2022-10-16 09:24:41', '2022-10-16 09:24:41');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `name`, `email`, `message`, `createdAt`, `updatedAt`) VALUES
(1, 'fdsfd', 'fdsfd@dsjk.co', 'fdsfd', '2022-10-16 15:14:17', '2022-10-16 15:14:17'),
(3, 'fdsfd', 'rahmawnj3@gmail.com', 'fdsfd', '2022-10-16 15:38:13', '2022-10-16 15:38:13'),
(4, 'fdsfd', 'rahmawnj4@gmail.com', 'fdsfd', '2022-10-16 15:58:23', '2022-10-16 15:58:23'),
(5, 'fdsfd', 'rahmawnj4@gmail.com', 'fdsfd', '2022-10-16 15:58:23', '2022-10-16 15:58:23'),
(6, 'fdsfd', 'rahmawnj4@gmail.com', 'fdsfd', '2022-10-16 15:59:16', '2022-10-16 15:59:16'),
(7, 'fdsfd', 'rahmawnj4@gmail.com', 'fdsfd', '2022-10-16 16:12:30', '2022-10-16 16:12:30'),
(8, 'Rahma', 'rahmawnj4@gmail.com', 'ddsassdasdsdasd', '2022-10-16 16:14:27', '2022-10-16 16:14:27'),
(9, 'Rahma', 'mfathpradana@gmail.com', 'fdsdsfdsfsd', '2022-10-17 00:44:39', '2022-10-17 00:44:39'),
(10, 'Rahma', 'mfathpradana@gmail.com', 'hhfjfjgjh\n', '2022-10-17 00:46:28', '2022-10-17 00:46:28'),
(11, 'Rahma', 'mfathpradana@gmail.com', 'kjjgkhjjj\n', '2022-10-17 00:47:31', '2022-10-17 00:47:31'),
(12, 'Rahma Janah', 'rahmawnj4@gmail.com', 'cassaasf', '2022-10-17 00:50:06', '2022-10-17 00:50:06'),
(13, 'Rahma Janah', 'rahmawnj4@gmail.com', 'nvngfj', '2022-10-17 00:51:24', '2022-10-17 00:51:24'),
(14, 'Rahma Janah', 'rahmawnj4@gmail.com', 'HALLOOOOOO', '2022-10-17 00:56:04', '2022-10-17 00:56:04'),
(15, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dassasadsasdsasda HALLO\n', '2022-10-17 00:58:54', '2022-10-17 00:58:54'),
(16, 'Rahma Janah', 'rahmawnj4@gmail.com', 'ddssdsfsd', '2022-10-17 00:59:57', '2022-10-17 00:59:57'),
(17, 'Rahma Janah', 'rahmawnj4@gmail.com', 'fdsfddsfds', '2022-10-17 01:00:29', '2022-10-17 01:00:29'),
(18, 'Rahma', 'rahmawnj3@gmail.com', 'fdsfdsfsfsd', '2022-10-17 01:01:45', '2022-10-17 01:01:45'),
(19, 'Rahma', 'rahmawnj3@gmail.com', 'hgjgkjlhkj\n', '2022-10-17 01:02:15', '2022-10-17 01:02:15'),
(20, 'Rahma Janah', 'rahmawnj4@gmail.com', 'uyliyo', '2022-10-17 01:03:03', '2022-10-17 01:03:03'),
(21, 'Rahma Janah', 'rahmawnj4@gmail.com', 'HALLOO RAHMA', '2022-10-17 01:03:56', '2022-10-17 01:03:56'),
(22, 'Rahma', 'rahmawnj4@gmail.com', 'HAI\n', '2022-10-17 01:05:18', '2022-10-17 01:05:18'),
(23, 'Rahma Janah', 'rahmawnj4@gmail.com', 'asdssadsas', '2022-10-17 03:51:31', '2022-10-17 03:51:31'),
(24, 'Rahma Janah', 'rahmawnj4@gmail.com', 'ghjhk', '2022-10-17 03:54:50', '2022-10-17 03:54:50'),
(25, 'Rahma', 'rahmawnj3@gmail.com', 'dsasa', '2022-10-17 03:56:35', '2022-10-17 03:56:35'),
(26, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dadsa', '2022-10-17 04:01:04', '2022-10-17 04:01:04'),
(27, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dsadsdsa', '2022-10-17 04:02:23', '2022-10-17 04:02:23'),
(28, 'Rahma Janah', 'rahmawnj4@gmail.com', 'hjkhkj', '2022-10-17 04:05:32', '2022-10-17 04:05:32'),
(29, 'Rahma Janah', 'rahmawnj4@gmail.com', 'ddssass', '2022-10-17 04:06:54', '2022-10-17 04:06:54'),
(30, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dssadssa', '2022-10-17 04:07:18', '2022-10-17 04:07:18'),
(31, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dasdsdsasa', '2022-10-17 04:07:55', '2022-10-17 04:07:55'),
(32, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dsasaddasa', '2022-10-17 04:08:24', '2022-10-17 04:08:24'),
(33, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dasd', '2022-10-17 04:09:13', '2022-10-17 04:09:13'),
(34, 'Rahma', 'mfathpradana@gmail.com', 'dsasdsa', '2022-10-17 04:09:48', '2022-10-17 04:09:48'),
(35, 'Rahma', 'mfathpradana@gmail.com', 'dassda', '2022-10-17 04:10:09', '2022-10-17 04:10:09'),
(36, 'Rahma Janah', 'rahmawnj4@gmail.com', 'sasa', '2022-10-17 04:14:30', '2022-10-17 04:14:30'),
(37, 'Rahma Janah', 'rahmawnj4@gmail.com', 'fsdfs', '2022-10-17 04:26:03', '2022-10-17 04:26:03'),
(38, 'Rahma Janah', 'rahmawnj4@gmail.com', 'vsddsds', '2022-10-17 04:26:22', '2022-10-17 04:26:22'),
(39, 'Rahma Janah', 'rahmawnj4@gmail.com', 'eqrerwe', '2022-10-17 04:26:59', '2022-10-17 04:26:59'),
(40, 'Rahma Janah', 'rahmawnj4@gmail.com', 'vx vbv', '2022-10-17 04:36:51', '2022-10-17 04:36:51'),
(41, 'Rahma Janah', 'rahmawnj4@gmail.com', 'gdff\n', '2022-10-17 04:37:16', '2022-10-17 04:37:16'),
(42, 'Rahma', 'rahmawnj4@gmail.com', 'hghj', '2022-10-17 04:38:09', '2022-10-17 04:38:09'),
(43, 'Rahma Janah', 'rahmawnj4@gmail.com', 'ffgfd', '2022-10-17 04:41:23', '2022-10-17 04:41:23'),
(44, 'Rahma Janah', 'rahmawnj4@gmail.com', 'gdfgdf', '2022-10-17 04:41:49', '2022-10-17 04:41:49'),
(45, 'Rahma Janah', 'rahmawnj4@gmail.com', 'cxsdf', '2022-10-17 04:42:22', '2022-10-17 04:42:22'),
(46, 'Rahma Janah', 'rahmawnj4@gmail.com', 'sddggdf', '2022-10-17 04:42:55', '2022-10-17 04:42:55'),
(47, 'Rahma Janah', 'rahmawnj4@gmail.com', 'fsd', '2022-10-17 04:43:18', '2022-10-17 04:43:18'),
(48, 'Rahma Janah', 'rahmawnj4@gmail.com', 'fsfdsfs', '2022-10-17 04:43:47', '2022-10-17 04:43:47'),
(49, 'Rahma', 'mfathpradana@gmail.com', 'wrrew', '2022-10-17 04:45:01', '2022-10-17 04:45:01'),
(50, 'Rahma Janah', 'rahmawnj4@gmail.com', 'jjgkgj', '2022-10-17 04:52:49', '2022-10-17 04:52:49'),
(51, 'Rahma Janah', 'rahmawnj4@gmail.com', 'gfd', '2022-10-17 04:54:52', '2022-10-17 04:54:52'),
(52, 'Rahma Janah', 'rahmawnj4@gmail.com', 'sdads', '2022-10-17 04:55:43', '2022-10-17 04:55:43'),
(53, 'Rahma', 'mfathpradana@gmail.com', 'jhhjgj', '2022-10-17 04:56:14', '2022-10-17 04:56:14'),
(54, 'Rahma Janah', 'rahmawnj4@gmail.com', 'bddyrtyrtyr', '2022-10-17 04:57:38', '2022-10-17 04:57:38'),
(55, 'Rahma', 'mfathpradana@gmail.com', 'sffds', '2022-10-17 05:44:57', '2022-10-17 05:44:57'),
(56, 'Fath', 'mfathpradana@gmail.com', 'Halo Rahma, Apakah km available', '2022-10-17 05:52:43', '2022-10-17 05:52:43'),
(57, 'sdhskhdjs', 'rahmawnj3@gmail.comfs', 'fsdfsd', '2022-10-30 07:03:36', '2022-10-30 07:03:36'),
(58, 'jhjhjhjhj', 'rahmawnj4@gmail.com', 'jjhjhj', '2022-10-30 07:07:20', '2022-10-30 07:07:20'),
(59, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dfdfddf', '2022-10-30 07:07:35', '2022-10-30 07:07:35'),
(60, 'Rahma Janah', 'rahmawnj4@gmail.com', 'HAII', '2022-10-30 07:11:11', '2022-10-30 07:11:11'),
(61, 'Rahma Janah', 'rahmawnj4@gmail.com', 'dsddssdsd', '2022-10-30 07:13:28', '2022-10-30 07:13:28'),
(62, 'Rahma Janah', 'rahmawnj4@gmail.com', 'trtrtrr', '2022-10-30 07:14:08', '2022-10-30 07:14:08'),
(63, 'Rahma Janah', 'rahmawnj4@gmail.com', 'fsdsffdsfds', '2022-10-30 07:15:29', '2022-10-30 07:15:29');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `senderId` varchar(255) NOT NULL,
  `receiverId` varchar(255) NOT NULL,
  `room` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `senderId`, `receiverId`, `room`, `time`, `message`, `createdAt`, `updatedAt`) VALUES
(1, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', 'fddfgf', '2022-10-24 18:43:32', '2022-10-24 18:43:32'),
(2, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '1:44', 'hemmmm', '2022-10-24 18:44:53', '2022-10-24 18:44:53'),
(3, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '1:44', 'hemmmm', '2022-10-24 18:44:54', '2022-10-24 18:44:54'),
(4, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '1:44', 'hemmmm', '2022-10-24 18:44:54', '2022-10-24 18:44:54'),
(5, '976339a9-430e-45b6-b765-d00a33275fb9', 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '2:9', 'HAI AKU MIKI', '2022-10-24 19:09:02', '2022-10-24 19:09:02'),
(6, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '2:9', 'HALOO', '2022-10-24 19:09:14', '2022-10-24 19:09:14'),
(7, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '20:48', 'hihih', '2022-10-25 13:48:26', '2022-10-25 13:48:26'),
(8, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '20:48', 'yoooiii', '2022-10-25 13:48:33', '2022-10-25 13:48:33'),
(9, 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '976339a9-430e-45b6-b765-d00a33275fb9', '20:49', 'yoiiiimoooo', '2022-10-25 13:49:44', '2022-10-25 13:49:44'),
(10, '976339a9-430e-45b6-b765-d00a33275fb9', 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '16:9', 'jijijiji', '2022-10-30 09:09:22', '2022-10-30 09:09:22'),
(11, '976339a9-430e-45b6-b765-d00a33275fb9', 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '16:11', 'hmmm', '2022-10-30 09:11:15', '2022-10-30 09:11:15'),
(12, '976339a9-430e-45b6-b765-d00a33275fb9', 'jht939483983u89r', '976339a9-430e-45b6-b765-d00a33275fb9', '16:12', 'hihi', '2022-10-30 09:12:44', '2022-10-30 09:12:44');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `name`, `description`, `createdAt`, `updatedAt`) VALUES
(1, 'name', 'Rahma', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(2, 'motto', 'Code Is An Art', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(3, 'role', 'Programmer', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(4, 'about', '<p><strong>Hallo!</strong></p><p>I&#x27;m Rahma, i like to do creative things such as making applications, simple games, drawings, 3d modeling, animation and others.</p><p>It&#x27;s a random thing. but I enjoy it!</p><p>I also available for freelance. </p', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(5, 'location', 'Bandung', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(6, 'email', 'rahmawnj3@gmail.com', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(7, 'contact', '089637761500', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(8, 'image', '5ce7001d6b6027bb5c631dd76ce43bb2.jpg', '2022-10-08 14:10:34', '2022-10-17 00:51:15'),
(9, 'email_password', 'hhilkbtwubcrhypt', '2022-10-17 02:29:53', '2022-10-17 00:51:15'),
(10, 'socketId', 'jht939483983u89r', '2022-10-24 18:38:46', '2022-10-24 18:38:46');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `uuid`, `name`, `description`, `status`, `tags`, `image`, `createdAt`, `updatedAt`) VALUES
(6, 'd4cb95da-da53-4fee-8aba-e77218ceddeb', 'rwerewrewrew', '<p>&lt;slfjslkjflksjlkfjsdlkfds</p>', 'Publish', 'rewewr', 'ab2f4e8af244537b60c22c73fcc8020e.png', '2022-10-16 09:04:02', '2022-10-16 09:08:19'),
(8, 'e246d464-4733-478e-a73f-5f70e0c69db7', '5445', '<p>gggffdg</p>', 'Draft', 'gfdgfd', 'fb6b8fc09b78b59c04f36f2d2cb69471.jpg', '2022-10-16 09:10:46', '2022-10-16 09:10:46');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `sid` varchar(36) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `data` text DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`sid`, `expires`, `data`, `createdAt`, `updatedAt`) VALUES
('90a31JYIpILU2eJL5LTN2m0IMyylw6F5', '2022-11-01 00:43:04', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-31 00:43:04', '2022-10-31 00:43:04'),
('jhL_fcegjvJQPklwweK2K1JA4AFKpR8g', '2022-11-01 00:43:03', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-31 00:43:03', '2022-10-31 00:43:03'),
('K7xM8dXQmUs7jPM2OuCmxK2ERs3NFmL0', '2022-11-01 00:43:04', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-31 00:43:04', '2022-10-31 00:43:04'),
('knD-iPJIuOiyb9_ThCXlDOQhyGmexLx3', '2022-10-31 14:26:26', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-30 14:26:26', '2022-10-30 14:26:26'),
('leFwiTuGErZ231ufuPgL2ucTe6TUmCH0', '2022-10-31 14:57:22', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-30 14:57:22', '2022-10-30 14:57:22'),
('OpN5s2tcEdRf2AlaJJ8LyB43f0-Ixd23', '2022-10-31 14:57:23', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-30 14:57:23', '2022-10-30 14:57:23'),
('q0zeN_FK1QsZKl7H7ZmkYUS4MhhyAS2l', '2022-10-31 14:26:26', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-30 14:26:26', '2022-10-30 14:26:26'),
('Ru0keIgEVOWYdxzap_FmzZUqblATFkRT', '2022-11-01 00:43:02', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-31 00:43:03', '2022-10-31 00:43:03'),
('SOpH-7N6Osj1wUAgx7uQln-EaXKe7e4y', '2022-10-31 14:26:26', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-30 14:26:26', '2022-10-30 14:26:26'),
('sx6m0h2iNwUP4G7qJ6uJARG2q1v9qCJw', '2022-10-31 15:15:43', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"976339a9-430e-45b6-b765-d00a33275fb9\"}', '2022-10-30 14:57:23', '2022-10-30 15:15:43'),
('xjDHCmif8ksT0ImyGBvmeZmXlKSRmAkE', '2022-11-01 00:43:02', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-31 00:43:02', '2022-10-31 00:43:02'),
('YXh9xaBIrvcaX0-qonIpeSYaKpBhMArI', '2022-10-31 14:26:26', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2022-10-30 14:26:26', '2022-10-30 14:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `progress` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `uuid`, `name`, `description`, `progress`, `createdAt`, `updatedAt`) VALUES
(4, '4a8fac4f-4426-4148-9112-9143b19dac3f', 'PHP', '-\n', '48', '2022-10-14 11:25:48', '2022-10-14 11:25:48'),
(5, '19d80c26-865b-4ccc-8530-3897abc6977c', 'Javascript', '-', '21', '2022-10-14 11:26:09', '2022-10-14 11:26:09'),
(6, '82c94f95-06b2-45b1-b3db-61861d0eb99b', 'HTML', '-', '100', '2022-10-14 11:26:29', '2022-10-14 11:26:29'),
(7, 'e32704d8-f199-4ca5-bb26-db27f8ed03f8', 'CSS', '-', '81', '2022-10-14 11:26:51', '2022-10-14 11:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `socialmedias`
--

CREATE TABLE `socialmedias` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `socialmedias`
--

INSERT INTO `socialmedias` (`id`, `uuid`, `platform`, `link`, `icon`, `status`, `createdAt`, `updatedAt`) VALUES
(8, '276ec9af-7b78-4cc8-a612-5770d5a4ab5a', 'Instagram', 'https://instagram.com/rhm_nj?igshid=YmMyMTA2M2Y=', 'fa-instagram', 'Publish', '2022-10-16 12:27:43', '2022-10-16 12:27:43'),
(9, '2c728cbb-61d2-4936-aebf-f38c242741f2', 'whatsapp', 'https://wa.me/6289637761500', 'fa-whatsapp', 'Publish', '2022-10-16 12:28:06', '2022-10-16 12:28:06'),
(11, '5cc5e513-2d78-425f-b9ff-439a06bb8282', 'Gitlab', 'https://gitlab.com/rahmawnj_', 'fa-gitlab', 'Publish', '2022-10-16 12:28:48', '2022-10-16 12:28:48'),
(12, '41f9ebf6-0125-4d66-9465-cc172ed0aece', 'Linkedin', '-', 'fa-linkedin', 'Draft', '2022-10-16 12:31:44', '2022-10-16 12:31:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `password`, `role`, `createdAt`, `updatedAt`) VALUES
(1, 'a07e8a63-2107-4620-8ca2-7fcc16a2a698', 'Rahma', 'rahmawnj3@gmail.com', '$argon2id$v=19$m=4096,t=3,p=1$PMHtk7a5FJ5Wgy+luTAEgQ$OJL4kdIveQpOP4to/nfOqvFrme2dF8MNShVBDh9D27o', 'admin', '2022-10-06 10:03:16', '2022-10-06 10:03:16'),
(5, 'a07e8a63-2107-4620-8ca2-7fcc16a2a65', 'ama', 'rahmawnj4@gmail.com', '$argon2id$v=19$m=4096,t=3,p=1$9rdPl/J+W9Bp+vt1XSn8ow$B9vk289FJ3qftboStpQt0P3ZZAcqF2WdFxS9Hgis/6Q', 'user\r\n', '2022-10-06 10:03:16', '2022-10-23 18:55:12'),
(6, '976339a9-430e-45b6-b765-d00a33275fb9', 'Miki', 'miki@gmail.com', '$argon2id$v=19$m=4096,t=3,p=1$53Rr9TqKQV4kyRVLqyd9zg$cHtF/BSiKmZ2x7mb60c2xNuOsIedbjbLkVcZ3mN4F8s', 'User', '2022-10-24 17:14:21', '2022-10-24 17:14:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socialmedias`
--
ALTER TABLE `socialmedias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `socialmedias`
--
ALTER TABLE `socialmedias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
